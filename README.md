# Visual stimulation program for Vuzix Blade augmented-reality glasses

This program presents simple visual stimuli (colored geometric shapes) contingent on the auditory environment as captured by the glasses' integrated microphone. A custom-trained neural network (tflite) detects the presence of tones corresponding to certain notes on the Western scale, e.g. a middle C at 262 Hz, and presents the corresponding shape if the detection threshold is surpassed.

## Features

- Implemented entirely in C++ using the Android NDK. Many parts in the graphics section were adapted from Sylvain Ratabouil's highly useful book "Android NDK Beginner's Guide" (2012).
- Input gets transformed by a custom wavelet convolution routine on multiple threads. FFT routines from https://www.nayuki.io/
- Sound/shape mappings can be changed by config file (`/app/src/main/assets/ARS.cfg`)

## Dependencies

- TensorFlow lite (tflite)
- Google Oboe
- Android NDK version 21.1.6352462
- GLES2/EGL

## Structure

- Android stuff like state changes gets handled by `AugmentedRealityStimulation`, which implements the `ActivityHandler` interface
- Graphics and sound are handled by `GraphicsManagerGLES2` and `SoundManagerOboe`, respectively.
- `StimulationManager` is the class at the center of everything to do with tone detection (`ToneDetection`) and stimulus presentation (`StimulusControl`).

## How to use

Build with Android Studio and deploy the .apk to an Android device. The application may require permission to access the device's microphone and hard disk write access.
