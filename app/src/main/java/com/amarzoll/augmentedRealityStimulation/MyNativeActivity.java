package com.amarzoll.augmentedRealityStimulation;

public class MyNativeActivity extends android.app.NativeActivity
 {
    static {
        System.loadLibrary("tensorflowlite");
        System.loadLibrary("augmentedRealityStimulation");
    }
}