#ifndef NDKAUGMENTEDREALITYSTIMULATION_SHAPEFORM_H
#define NDKAUGMENTEDREALITYSTIMULATION_SHAPEFORM_H

#include <map>
#include <string>

enum class ShapeForm {
    TRIANGLE,
    SQUARE,
    CIRCLE,
    STAR,
    PENTAGON,
    CROSS,
    CRESCENT,
    SEMICIRCLE
};

const std::map<std::string, ShapeForm> str2shapeform = {
        {"TRIANGLE", ShapeForm::TRIANGLE},
        {"SQUARE", ShapeForm::SQUARE},
        {"CIRCLE", ShapeForm::CIRCLE},
        {"STAR", ShapeForm::STAR},
        {"PENTAGON", ShapeForm::PENTAGON},
        {"CROSS", ShapeForm::CROSS},
        {"CRESCENT", ShapeForm::CRESCENT},
        {"SEMICIRCLE", ShapeForm::SEMICIRCLE}
};

#endif //NDKAUGMENTEDREALITYSTIMULATION_SHAPEFORM_H
