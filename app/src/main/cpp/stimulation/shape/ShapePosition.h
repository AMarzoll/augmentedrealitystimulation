#ifndef NDKAUGMENTEDREALITYSTIMULATION_SHAPEPOSITION_H
#define NDKAUGMENTEDREALITYSTIMULATION_SHAPEPOSITION_H

#include <map>
#include <string>

enum class ShapePosition {
    UPPER_LEFT,
    UPPER_MID,
    UPPER_RIGHT,
    CENTER_RIGHT,
    LOWER_RIGHT,
    LOWER_MID,
    LOWER_LEFT,
    CENTER_LEFT,

    HEPTAGON1,
    HEPTAGON2,
    HEPTAGON3,
    HEPTAGON4,
    HEPTAGON5,
    HEPTAGON6,
    HEPTAGON7
};

const std::map<std::string, ShapePosition> str2shapeposition = {
        {"UPPER_LEFT", ShapePosition::UPPER_LEFT},
        {"UPPER_MID", ShapePosition::UPPER_MID},
        {"UPPER_RIGHT", ShapePosition::UPPER_RIGHT},
        {"CENTER_RIGHT", ShapePosition::CENTER_RIGHT},
        {"LOWER_RIGHT", ShapePosition::LOWER_RIGHT},
        {"LOWER_MID", ShapePosition::LOWER_MID},
        {"LOWER_LEFT", ShapePosition::LOWER_LEFT},
        {"CENTER_LEFT", ShapePosition::CENTER_LEFT},

        {"HEPTAGON1",   ShapePosition::HEPTAGON1},
        {"HEPTAGON2",   ShapePosition::HEPTAGON2},
        {"HEPTAGON3",   ShapePosition::HEPTAGON3},
        {"HEPTAGON4",   ShapePosition::HEPTAGON4},
        {"HEPTAGON5",   ShapePosition::HEPTAGON5},
        {"HEPTAGON6",   ShapePosition::HEPTAGON6},
        {"HEPTAGON7",   ShapePosition::HEPTAGON7},
};


#endif //NDKAUGMENTEDREALITYSTIMULATION_SHAPEPOSITION_H
