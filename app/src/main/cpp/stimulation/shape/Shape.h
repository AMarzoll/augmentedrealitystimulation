#ifndef NDKAUGMENTEDREALITYSTIMULATION_SHAPE_H
#define NDKAUGMENTEDREALITYSTIMULATION_SHAPE_H

#include "ShapeColor.h"
#include "ShapePosition.h"
#include "ShapeForm.h"

class Shape {
public:
    ShapeForm       form_;
    ShapeColor      color_;
    ShapePosition   position_;
    float           scalingFactor_;
};

#endif //NDKAUGMENTEDREALITYSTIMULATION_SHAPE_H
