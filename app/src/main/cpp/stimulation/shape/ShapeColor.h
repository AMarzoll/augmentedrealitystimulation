#ifndef NDKAUGMENTEDREALITYSTIMULATION_SHAPECOLOR_H
#define NDKAUGMENTEDREALITYSTIMULATION_SHAPECOLOR_H

#include <map>
#include <string>

enum class ShapeColor {
    RED,
    GREEN,
    BLUE,
    YELLOW,
    PURPLE,
    ORANGE,
    CYAN,
    WHITE,
    DARK_GREEN,
    BROWN
};

const std::map<std::string, ShapeColor> str2shapecolor = {
        {"RED", ShapeColor::RED},
        {"GREEN", ShapeColor::GREEN},
        {"BLUE", ShapeColor::BLUE},
        {"YELLOW", ShapeColor::YELLOW},
        {"PURPLE", ShapeColor::PURPLE},
        {"ORANGE", ShapeColor::ORANGE},
        {"CYAN", ShapeColor::CYAN},
        {"WHITE", ShapeColor::WHITE},
        {"DARK_GREEN", ShapeColor ::DARK_GREEN},
        {"BROWN", ShapeColor::BROWN}
};

#endif //NDKAUGMENTEDREALITYSTIMULATION_SHAPECOLOR_H
