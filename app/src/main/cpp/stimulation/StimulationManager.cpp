#include "StimulationManager.h"

#include <sstream>
#include <chrono>

#include "../utility/Log.h"
#include "../GlobalParameters.h"
#include "../utility/FileOperations.h"
#include "../graphics/GLES2/GraphicsManagerGLES2.h"


StimulationManager::StimulationManager(
        android_app* ptrApp,
        GraphicsManagerGLES2& graphicsManager,
        SoundManagerOboe& soundManager) :
    ptrApp_(ptrApp),
    graphicsManager_(graphicsManager),
    soundManager_(soundManager),
    toneDetection_(ptrApp, *this),
    stimulusControl_(*this){
}



void
StimulationManager::start() {
    createStimuli();
    for(auto& s : stimuli_) {
        //register Detector component
        s.setDetectorID(nextDetectorID_);
        s.detector_.init(s.getF0());
        toneDetection_.waveletConvolutionManager_.createConvolutions(nextDetectorID_);
        toneDetection_.registerDetector(nextDetectorID_);
        ++nextDetectorID_;

        //register ShapeGLES2 component
        s.makeGLES2Shape();
        s.setShapeGLES2ID(nextShapeGLES2ID_);
        graphicsManager_.registerShape(nextShapeGLES2ID_);
        ++nextShapeGLES2ID_;

        //register Tracker component
        s.setTrackerID(nextTrackerID_);
        stimulusControl_.registerTracker(nextTrackerID_);
        ++nextTrackerID_;
    }
    toneDetection_.launchDetectionThread();
    toneDetection_.waveletConvolutionManager_.launchWorkerThreads();
    registeredComponents_ = true;
}



void
StimulationManager::update() {
    //return if shapes are not ready yet
    if(!registeredComponents_) {return;}
    //if convolution results are available, update trackers
    if(toneDetection_.detectionComplete_) {
        toneDetection_.detectionComplete_ = false;
        std::map<TrackerID, bool> inferenceResults;
        for(auto& s : stimuli_) {
            inferenceResults.emplace(s.fetchInferenceResult());
        }
        stimulusControl_.advanceAllTrackers(inferenceResults);
    }
    for(auto& s : stimuli_) {
        s.updateRenderStatus();
    }
}



void
StimulationManager::stop() {

}



void
StimulationManager::createStimuli() {
    Log::info("Entered parseStimulusfile()");
    int bufSz = 0;
    auto buffer = loadAssetFile(ptrApp_, "ARS.cfg", bufSz);
    std::stringstream ss;
    ss << buffer.get();
    Log::info("Size of stringstream contents: %i", ss.str().size());
    auto current = stimuli_.rbegin();

    auto readStrFromStream = [&ss](){std::string val; ss >> val; return val;};
    auto readI64FromStream = [&ss](){int64_t val; ss >> val; return val;};

    while(!ss.eof()) {
        char token;
        std::string keyword;
        ss.get(token);
        switch(token) {
            case '$': {
                ss >> keyword;
                if(keyword == "STIMULUS") {
                    //new stimulus
                    stimuli_.emplace_back(Stimulus());
                    current = stimuli_.rbegin();
                }
                break;
            }
            case '#': {
                ss >> keyword;
                if(keyword == "NAME") {
                    std::string id;
                    ss >> id;
                    current->setID(id);
                } else if(keyword == "AUDIO_FREQUENCY_HZ") {
                    float f0Hz;
                    ss >> f0Hz;
                    current->setF0(f0Hz);
                } else if(keyword == "FORM") {
                    current->setForm(str2shapeform.at(readStrFromStream()));
                } else if(keyword == "COLOR") {
                    current->setColor(str2shapecolor.at(readStrFromStream()));
                } else if(keyword == "POSITION") {
                    current->setPosition(str2shapeposition.at(readStrFromStream()));
                } else if(keyword == "SCALING_FACTOR") {
                    float sf;
                    ss >> sf;
                    current->setScalingFactor(sf);
                    current->printToLog();
                } else if(keyword == "PULSE_DURATION_USEC") {
                    //current->pulseDuration_= std::chrono::microseconds(readI64FromStream());
                } else if(keyword == "PULSE_SOA_USEC") {
                    //current->pulseSoa_ = std::chrono::microseconds(readI64FromStream());
                } else if(keyword == "TRAIN_DURATION_USEC") {
                    //current->trainDuration_ = std::chrono::microseconds(readI64FromStream());
                } else if(keyword == "INTERTRAIN_DURATION_USEC") {
                    //current->intertrainDuration_ = std::chrono::microseconds(readI64FromStream());
                } else {
                    Log::warn("StimulationManager::parseStimulusfile(): Invalid keyword!");
                }
                break;
            }
            default:
                continue;
        }
    }
    Log::info("Finished StimulationManager::parseStimulusfile");
    assert(!stimuli_.empty()); //TODO: exception instead of assertion
}



struct DetectorNotFound : std::exception {};

Detector&
StimulationManager::accessDetector(DetectorID detectorID) {
    for(auto& s : stimuli_) {
        if(s.getDetectorID() == detectorID) {
            return s.detector_;
        }
    }
    throw DetectorNotFound();
}



struct ShapeGLES2NotFound : std::exception {};

std::unique_ptr<ShapeGLES2>&
StimulationManager::accessShapeGLES2(ShapeGLES2ID shapeGLES2ID) {
    for(auto& s : stimuli_) {
        if(s.getShapeGLES2ID() == shapeGLES2ID) {
            return s.shapeGLES2_;
        }
    }
    throw ShapeGLES2NotFound();
}



bool
StimulationManager::isNewAudioAvailable() {
    return soundManager_.isNewDataAvailable();
}



struct TrackerNotFound : std::exception{};

Tracker&
StimulationManager::accessTracker(TrackerID trackerID) {
    for(auto& s : stimuli_) {
        if(s.getTrackerID() == trackerID) {
            return s.tracker_;
        }
    }
    throw TrackerNotFound();
}

std::vector<int16_t> StimulationManager::getAudio() {return soundManager_.getAudio();}
