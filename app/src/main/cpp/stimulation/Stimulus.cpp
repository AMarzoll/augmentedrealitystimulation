#include "Stimulus.h"
#include "../utility/Log.h"


Stimulus::Stimulus(){
}


void Stimulus::makeGLES2Shape() {
    shapeGLES2_ = std::make_unique<ShapeGLES2>(shape_);
}



void
Stimulus::printToLog() {
    Log::info("STIMULUS");
    Log::info("id: %s\n", id_.c_str());
    Log::info("audioFrequencyHz: %f\n", f0Hz_);
    Log::info("Shape::form: %i\n", shape_.form_);
    Log::info("Shape::position: %i\n", shape_.position_);
    Log::info("Shape::color: %i\n", shape_.color_);
    Log::info("Shape::scalingFactor: %f\n", shape_.scalingFactor_);
    /*
    Log::info("pulseDuration: %i\n", pulseDuration_.count());
    Log::info("pulseSoa: %i\n", pulseSoa_.count());
    Log::info("trainDuration: %i\n", trainDuration_.count());
    Log::info("intertrainDuration: %i\n", intertrainDuration_.count());
    */
}



void
Stimulus::setDetectorID(DetectorID detectorID) {
    detector_.setDetectorID(detectorID);
}



void
Stimulus::setShapeGLES2ID(ShapeGLES2ID shapeGLES2ID) {
    shapeGLES2_->shapeGles2ID_ = shapeGLES2ID;
}



void
Stimulus::setTrackerID(TrackerID trackerID) {
    tracker_.setTrackerID(trackerID);
}



std::pair<TrackerID, bool>
Stimulus::fetchInferenceResult() {
    return std::make_tuple(this->tracker_.id_, this->detector_.nnInfResult_);
}



void
Stimulus::updateRenderStatus() {
    shapeGLES2_->setRenderStatus(tracker_.renderNextFrame_);
}



DetectorID
Stimulus::getDetectorID() {
    return detector_.getDetectorID();
}



ShapeGLES2ID
Stimulus::getShapeGLES2ID() {
    return shapeGLES2_->shapeGles2ID_;
}

TrackerID
Stimulus::getTrackerID() {
    return tracker_.getTrackerID();
}

void Stimulus::setScalingFactor(float scalingFactor) {shape_.scalingFactor_ = scalingFactor;}

void Stimulus::setPosition(ShapePosition position) {shape_.position_ = position;}

void Stimulus::setForm(ShapeForm form) {shape_.form_ = form;}

void Stimulus::setColor(ShapeColor color) {shape_.color_ = color;}

void Stimulus::setF0(float f0Hz) {f0Hz_ = f0Hz;}

void Stimulus::setID(StimulusID id) {id_ = id;}

StimulusID Stimulus::getID() {return id_;}

const Shape &Stimulus::getShape() {return shape_;}

float Stimulus::getF0() {return f0Hz_;}


