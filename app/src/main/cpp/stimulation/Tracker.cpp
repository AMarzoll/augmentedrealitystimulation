#include "Tracker.h"

Tracker::Tracker() {
    segmentTracker_.resize(trackerMaxSize_);
}



void Tracker::advanceTracker(bool isTonePresent) {
    if(segmentTracker_.size() >= trackerMaxSize_) {
        segmentTracker_.pop_front();
    }
    segmentTracker_.push_back(isTonePresent);
    updateRenderStatus();
}



void Tracker::updateRenderStatus() {
    //TODO: more sophisticated algorithm for intermittent high-frequency stimulation
    renderNextFrame_ = segmentTracker_.back();
}

TrackerID Tracker::getTrackerID() {return id_;}

void Tracker::setTrackerID(TrackerID trackerID) {id_ = trackerID;}