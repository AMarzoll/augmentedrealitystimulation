#include "StimulusControl.h"

#include "Tracker.h"
#include "StimulationManager.h"

StimulusControl::StimulusControl(StimulationManager& stimulationManager):
    stimulationManager_(stimulationManager){

}


void
StimulusControl::registerTracker(TrackerID trackerID) {
    trackers_.push_back(trackerID);
}



void
StimulusControl::advanceAllTrackers(const std::map<TrackerID, bool>& inferenceResults) {
    for(auto i : inferenceResults) {
        Tracker& tracker = stimulationManager_.accessTracker(i.first);
        tracker.advanceTracker(i.second);
    }
}


