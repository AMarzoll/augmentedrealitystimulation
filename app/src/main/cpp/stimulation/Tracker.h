#ifndef NDKAUGMENTEDREALITYSTIMULATION_TRACKER_H
#define NDKAUGMENTEDREALITYSTIMULATION_TRACKER_H

#include <deque>

#include "../GlobalParameters.h"

class Stimulus;

//component of Stimulus
class Tracker {
    friend class Stimulus;
public:
    Tracker();
    void advanceTracker(bool isTonePresent);

private:
    TrackerID getTrackerID();
    void setTrackerID(TrackerID trackerID);
    void updateRenderStatus();

    TrackerID id_;
    static constexpr uint16_t trackerMaxSize_ = (10.0f / Global::frameDurSec) + 0.5f; //round(10 / Global::frameDurSec)
    std::deque<bool> segmentTracker_;
    bool renderNextFrame_ = false;
};


#endif //NDKAUGMENTEDREALITYSTIMULATION_TRACKER_H
