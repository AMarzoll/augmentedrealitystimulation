#ifndef NDKAUGMENTEDREALITYSTIMULATION_STIMULUS_H
#define NDKAUGMENTEDREALITYSTIMULATION_STIMULUS_H

#include <string>
#include "shape/Shape.h"
#include "../tone_detection/Detector.h"
#include "../graphics/GLES2/ShapeGLES2.h"
#include "Tracker.h"
//#include "StimulationManager.h"
#include "../GlobalParameters.h"

class StimulationManager;

class Stimulus {
    friend class StimulationManager;
public:
                    Stimulus();
    StimulusID      getID();
    const Shape&    getShape();
    float           getF0();
    DetectorID      getDetectorID();
    ShapeGLES2ID    getShapeGLES2ID();
    TrackerID       getTrackerID();

    void            printToLog();

private:
    void                        makeGLES2Shape();
    std::pair<TrackerID, bool>  fetchInferenceResult();
    void                        updateRenderStatus();

    void            setID(StimulusID id);
    void            setF0(float f0Hz);
    void            setColor(ShapeColor color);
    void            setForm(ShapeForm form);
    void            setPosition(ShapePosition position);
    void            setScalingFactor(float scalingFactor);
    void            setDetectorID(DetectorID detectorID);
    void            setShapeGLES2ID(ShapeGLES2ID shapeGLES2ID);
    void            setTrackerID(TrackerID trackerID);

    StimulusID id_;
    Shape shape_;
    std::unique_ptr<ShapeGLES2> shapeGLES2_;
    Detector detector_;
    Tracker tracker_;

    float f0Hz_;
};




#endif //NDKAUGMENTEDREALITYSTIMULATION_STIMULUS_H
