#ifndef NDKAUGMENTEDREALITYSTIMULATION_STIMULUSCONTROL_H
#define NDKAUGMENTEDREALITYSTIMULATION_STIMULUSCONTROL_H

#include <vector>
#include <map>
//#include "Tracker.h"
//#include "StimulationManager.h"
#include "../GlobalParameters.h"

class StimulationManager;

class StimulusControl {
    friend class StimulationManager;
public:
    StimulusControl(StimulationManager& stimulationManager);
private:
    int temp_ = 0;
    StimulationManager& stimulationManager_;
    void registerTracker(TrackerID trackerID);
    void advanceAllTrackers(const std::map<TrackerID, bool>& inferenceResults);
    std::vector<TrackerID> trackers_;
};


#endif //NDKAUGMENTEDREALITYSTIMULATION_STIMULUSCONTROL_H
