#ifndef NDKAUGMENTEDREALITYSTIMULATION_STIMULATIONMANAGER_H
#define NDKAUGMENTEDREALITYSTIMULATION_STIMULATIONMANAGER_H

class Stimulus;
class Detector;
class Tracker;
class ShapeGLES2;

#include <android_native_app_glue.h>
#include <vector>
#include "../audio/oboe/SoundManagerOboe.h"
#include "../tone_detection/Detector.h"
#include "../graphics/GLES2/ShapeGLES2.h"
#include "Tracker.h"
#include "../tone_detection/ToneDetection.h"
#include "StimulusControl.h"
#include "Stimulus.h"
#include "../tone_detection/WaveletConvolutionManager.h"


class GraphicsManagerGLES2;

class StimulationManager {
public:
                                        StimulationManager(android_app* ptrApp,
                                                           GraphicsManagerGLES2& graphicsManager,
                                                           SoundManagerOboe& soundManager);
    void                                start();
    void                                update();
    void                                stop();

    Detector&                           accessDetector(DetectorID);
    std::unique_ptr<ShapeGLES2>&        accessShapeGLES2(ShapeGLES2ID);
    Tracker&                            accessTracker(TrackerID);

    bool                                isNewAudioAvailable();
    std::vector<int16_t>                getAudio();

private:
    void                                createStimuli();

    ToneDetection                       toneDetection_;
    StimulusControl                     stimulusControl_;

    android_app*                        ptrApp_ = nullptr;
    std::vector<Stimulus>               stimuli_;
    GraphicsManagerGLES2&               graphicsManager_;
    SoundManagerOboe&                   soundManager_;

    DetectorID                          nextDetectorID_ = 0;
    ShapeGLES2ID                        nextShapeGLES2ID_ = 0;
    TrackerID                           nextTrackerID_ = 0;
    bool                                registeredComponents_ = false;
};



#endif //NDKAUGMENTEDREALITYSTIMULATION_STIMULATIONMANAGER_H