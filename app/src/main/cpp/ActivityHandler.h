#ifndef ARS_ACTIVITYHANDLER_H
#define ARS_ACTIVITYHANDLER_H

//#include <cstddef>
#include "GlobalParameters.h"

class ActivityHandler {
public:
    virtual             ~ActivityHandler() = default;
    virtual Status      onActivate() = 0;
    virtual void        onDeactivate() = 0;
    virtual Status      onStep() = 0;
    virtual void        onStart() {};
    virtual void        onResume() {};
    virtual void        onPause() {};
    virtual void        onStop() {};
    virtual void        onDestroy() {};
    virtual void        onSaveInstanceState(void** ptrData, size_t* ptrSize) {};
    virtual void        onConfigurationChanged() {};
    virtual void        onLowMemory() {};
    virtual void        onCreateWindow() {};
    virtual void        onDestroyWindow() {};
    virtual void        onGainFocus() {};
    virtual void        onLostFocus() {};
};

#endif //ARS_ACTIVITYHANDLER_H
