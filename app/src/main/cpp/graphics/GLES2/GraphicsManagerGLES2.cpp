#include "GraphicsManagerGLES2.h"
#include "ShapeGLES2.h"
#include "../../utility/Log.h"
#include "../../utility/TimerServices.h"


GraphicsManagerGLES2::GraphicsManagerGLES2(android_app *ptrApp) :
    ptrApp_(ptrApp) {

}



void
GraphicsManagerGLES2::start() {
    initEGL();
    initGLES2();
    initShadersGLES2();
    Log::info("Finished GraphicsManagerGLES2::start");
}



void
GraphicsManagerGLES2::stop() {
    if(display_ != EGL_NO_DISPLAY) {
        eglMakeCurrent(display_, EGL_NO_SURFACE, EGL_NO_SURFACE, EGL_NO_CONTEXT);
        if(context_ != EGL_NO_CONTEXT) {
            eglDestroyContext(display_, context_);
            context_ = EGL_NO_CONTEXT;
        }
        if(surface_ != EGL_NO_SURFACE) {
            eglDestroySurface(display_, surface_);
            surface_ = EGL_NO_SURFACE;
        }
        eglTerminate(display_);
        display_ = EGL_NO_DISPLAY;
    }
}



void
GraphicsManagerGLES2::initEGL() {
    EGLint format;
    EGLint numConfigs;
    EGLConfig config;
    EGLBoolean isMadeCurrent = 0;
    EGLBoolean isSfcWidthValid = 0;
    EGLBoolean isSfcHeightValid = 0;
    bool isSfcWidthLT0 = true;
    bool isSfcHeightLT0 = true;
    //define display requirements, 16 bit mode
    const EGLint DISPLAY_ATTRIBS[] = {
            EGL_RENDERABLE_TYPE, EGL_OPENGL_ES2_BIT, //parameter name/value pairs
            EGL_BLUE_SIZE, 5,
            EGL_GREEN_SIZE, 6,
            EGL_RED_SIZE, 5,
            EGL_SURFACE_TYPE, EGL_WINDOW_BIT,
            EGL_NONE //terminates list
    };
    //request an OpenGL ES2 context
    const EGLint CONTEXT_ATTRIBS[] = {
            EGL_CONTEXT_CLIENT_VERSION, 2,
            EGL_NONE
    };

    display_ = eglGetDisplay(EGL_DEFAULT_DISPLAY);
    if(display_ == EGL_NO_DISPLAY) {
        Log::error("eglGetDisplay() failed.");
        goto ERROR;
    }
    if(!eglInitialize(display_, nullptr, nullptr)) {
        Log::error("eglInitialize() failed");
        goto ERROR;
    }
    if(!eglChooseConfig(display_, DISPLAY_ATTRIBS, &config, 1, &numConfigs) || (numConfigs <= 0)) {
        Log::error("eglChooseConfig failed");
        goto ERROR;
    }
    if(!eglGetConfigAttrib(display_, config, EGL_NATIVE_VISUAL_ID, &format)) {
        Log::error("eglGetConfigAttrib failed");
        goto ERROR;
    }
    ANativeWindow_setBuffersGeometry(ptrApp_->window, 0, 0, format);
    surface_ = eglCreateWindowSurface(display_, config, ptrApp_->window, nullptr);
    if(surface_ == EGL_NO_SURFACE) {
        Log::error("eglCreateWindowSurface failed");
        goto ERROR;
    }
    context_ = eglCreateContext(display_, config, nullptr, CONTEXT_ATTRIBS);
    if(context_ == EGL_NO_CONTEXT) {
        Log::error("eglCreateContext failed");
        goto ERROR;
    }
    isMadeCurrent    = eglMakeCurrent(display_, surface_, surface_, context_);
    isSfcWidthValid  = eglQuerySurface(display_, surface_, EGL_WIDTH, &renderWidth_);
    isSfcHeightValid = eglQuerySurface(display_, surface_, EGL_HEIGHT, &renderHeight_);
    isSfcWidthLT0    = (renderWidth_ <= 0);
    isSfcHeightLT0   = (renderHeight_ <= 0);
    if(!isMadeCurrent || !isSfcWidthValid || !isSfcHeightValid || isSfcWidthLT0 || isSfcHeightLT0) {
        Log::error("Final EGL sanity check failed");
        goto ERROR;
    }
    //TODO: experimental part
    eglPresentationTimeANDROID_ = reinterpret_cast<
            bool (*)(EGLDisplay, EGLSurface, khronos_stime_nanoseconds_t)>(
            eglGetProcAddress("eglPresentationTimeANDROID"));
    assert(eglPresentationTimeANDROID_);
    presentation_time_ = TimerServices::getCurrentTime();
    Log::info("Finished GraphicsManagerGLES2::initEGL");
    return;

    ERROR:
    Log::error("GraphicsManagerGLES2::initEGL - ERROR");
    this->stop();
}



void
GraphicsManagerGLES2::initGLES2() const {
    glViewport(0, 0, renderWidth_, renderHeight_);
    glDisable(GL_DEPTH_TEST);
    Log::info("Finished GraphicsManagerGLES2::initGLES2");

}



bool
GraphicsManagerGLES2::initShadersGLES2() {
    const char vShaderStr[] =
            "attribute vec4 aPosition;      \n"
            "attribute vec4 aColor;         \n"
            "varying vec4 vColor;           \n"
            "void main()                    \n"
            "{                              \n"
            "   gl_Position = aPosition;    \n"
            "   vColor = aColor;            \n"
            "}                              \n";

    const char fShaderStr[] =
            "precision mediump float;   \n"
            "varying vec4 vColor;       \n"
            "void main()                \n"
            "{                          \n"
            "   gl_FragColor = vColor;  \n"
            "}                          \n";

    GLuint vertexShader;
    GLuint fragmentShader;
    GLint linked;

    vertexShader = loadShader(vShaderStr, GL_VERTEX_SHADER);
    fragmentShader = loadShader(fShaderStr, GL_FRAGMENT_SHADER);

    programObject_ = glCreateProgram();
    if(programObject_ == 0) return false;
    glAttachShader(programObject_, vertexShader);
    glAttachShader(programObject_, fragmentShader);
    glBindAttribLocation(programObject_, 0, "aPosition"); //bind aPosition to attribute 0
    glBindAttribLocation(programObject_, 1, "aColor");
    glLinkProgram(programObject_);
    glGetProgramiv(programObject_, GL_LINK_STATUS, &linked);
    if(!linked) {
        GLint infoLen = 0;
        glGetProgramiv(programObject_, GL_INFO_LOG_LENGTH, &infoLen);
        if(infoLen > 1) {
            char* infoLog = static_cast<char*>(malloc(sizeof(char) * infoLen));
            glGetProgramInfoLog(programObject_, infoLen, nullptr, infoLog);
            Log::error("Error linking program:\n%s\n", infoLog);
            free(infoLog);
        }
        glDeleteProgram(programObject_);
        return false;
    }
    glClearColor(0.0F, 0.0F, 0.0F, 1.0F);
    Log::info("Finished GraphicsManagerGLES2::initShadersGLES2");
    return true;
}



GLuint
GraphicsManagerGLES2::loadShader(const char* shaderSrc, GLenum type) {
    GLuint shader;
    GLint compiled;

    //create shader object
    shader = glCreateShader(type);
    if(shader == 0) return 0;
    glShaderSource(shader, 1, &shaderSrc, nullptr);
    glCompileShader(shader);
    glGetShaderiv(shader, GL_COMPILE_STATUS, &compiled);
    if(!compiled) {
        GLint infoLen = 0;
        glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &infoLen);
        if(infoLen > 1) {
            char* infoLog = static_cast<char*>(malloc(sizeof(char) * infoLen));
            glGetShaderInfoLog(shader, infoLen, nullptr, infoLog);
            free(infoLog);
        }
        glDeleteShader(shader);
        return 0;
    }
    Log::info("Finished GraphicsManagerGLES2::loadShader");
    return shader;
}



void
GraphicsManagerGLES2::update() {
    //Log::info("Entering GraphicsManagerGLES2::update");
    glViewport(0, 0, renderWidth_, renderHeight_);
    glClear(GL_COLOR_BUFFER_BIT);
    glUseProgram(programObject_);
    this->drawShapes();
    //TODO: experimental part
    presentation_time_ += (1 / 60.0f) * 1e9;
    //Log::info("Designated pres. time: %" PRIi64, presentation_time_);
    eglPresentationTimeANDROID_(display_, surface_, presentation_time_);
    eglSwapBuffers(display_, surface_);
    //Log::info("Finished GraphicsManagerGLES2::update");
}



void
GraphicsManagerGLES2::drawShapes() {
    for(auto id : shapes_) {
        const auto& shapeGLES2 = stimulationManager_->accessShapeGLES2(id);
        if(!(shapeGLES2->getRenderStatus())) { continue; }
        std::vector<GLfloat> vertexVec = shapeGLES2->computeGLES2Vertices();
        std::array<GLfloat, 4> colorVec = shapeGLES2->getGLES2Color();
        glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, &vertexVec[0]);
        glVertexAttrib4fv(1, &colorVec[0]);
        glEnableVertexAttribArray(0);
//        glEnableVertexAttribArray(1);
        GLenum renderMethod = shapeGLES2->getGLES2RenderMethod();
        auto vSize = static_cast<GLsizei>(vertexVec.size() / 3);
        glDrawArrays(renderMethod, 0, vSize);
    }
}



void
GraphicsManagerGLES2::registerShape(ShapeGLES2ID shapeGLES2ID) {
    shapes_.push_back(shapeGLES2ID);
}



void
GraphicsManagerGLES2::registerStimulationManager(StimulationManager *stimulationManager) {
    stimulationManager_ = stimulationManager;
}


/*
void
GraphicsManagerGLES2::setRenderStatus(ShapeGLES2ID shapeGLES2ID, bool renderStatus) {
    stimulationManager_->accessShapeGLES2(shapeGLES2ID)->setRenderStatus(renderStatus);
}
*/


