#ifndef NDKAUGMENTEDREALITYSTIMULATION_GRAPHICSMANAGERGLES2_H
#define NDKAUGMENTEDREALITYSTIMULATION_GRAPHICSMANAGERGLES2_H

#include <android_native_app_glue.h>
#include <EGL/egl.h>
#include <GLES2/gl2.h>
#include <vector>

#include "ShapeGLES2.h"
#include "../../stimulation/StimulationManager.h"
//#include "../../AugmentedRealityStimulation.h"

class AugmentedRealityStimulation;

class GraphicsManagerGLES2 {
    friend class AugmentedRealityStimulation;
public:
                                        GraphicsManagerGLES2(android_app* ptrApp);
                                        ~GraphicsManagerGLES2() = default;

    void                                start();
    void                                stop();
    void                                update();
    void                                registerShape(ShapeGLES2ID shapeGLES2ID);
    //void                                setRenderStatus(ShapeGLES2ID shapeGLES2ID, bool renderStatus);
    void                                registerStimulationManager(StimulationManager* stimulationManager);

private:
    bool                                initShadersGLES2();
    void                                initEGL();
    void                                initGLES2() const;
    GLuint                              loadShader(const char* shaderSrc, GLenum type);
    void                                drawShapes();

    int32_t                             renderWidth_ = 0;
    int32_t                             renderHeight_ = 0;
    EGLDisplay                          display_ = EGL_NO_DISPLAY;
    EGLSurface                          surface_ = EGL_NO_SURFACE;
    EGLContext                          context_ = EGL_NO_CONTEXT;
    android_app*                        ptrApp_ = nullptr;
    GLuint                              programObject_ = 0;
    std::vector<ShapeGLES2ID>           shapes_;
    bool                                (*eglPresentationTimeANDROID_)(EGLDisplay dpy, EGLSurface sur, khronos_stime_nanoseconds_t time) = nullptr;
    int64_t                             presentation_time_ = 0;
    StimulationManager*                 stimulationManager_ = nullptr;
};

#endif //NDKAUGMENTEDREALITYSTIMULATION_GRAPHICSMANAGERGLES2_H
