#ifndef NDKAUGMENTEDREALITYSTIMULATION_SHAPEGLES2_H
#define NDKAUGMENTEDREALITYSTIMULATION_SHAPEGLES2_H

#include <vector>
#include <array>
#include <cstdint>
#include <GLES2/gl2.h>

#include "../../stimulation/shape/Shape.h"
#include "ShapeMapsGLES2.h"
#include "../../GlobalParameters.h"


//concrete graphics component of Stimulus
class ShapeGLES2 {
    friend class Stimulus;
public:
                                ShapeGLES2(Shape shape);
    std::vector<GLfloat>        computeGLES2Vertices();
    std::array<GLfloat, 4>      getGLES2Color();
    GLenum                      getGLES2RenderMethod();
    bool                        getRenderStatus();
    void                        setRenderStatus(bool newStatus);

private:
    ShapeGLES2ID                shapeGles2ID_;
    std::vector<GLfloat>        vertices_;
    std::array<GLfloat, 4>      color_;
    std::array<GLfloat, 3>      position_;
    GLenum                      renderMethod_;
    GLfloat                     scalingFactor_;
    bool                        renderStatus_;
};

#endif //NDKAUGMENTEDREALITYSTIMULATION_SHAPEGLES2_H