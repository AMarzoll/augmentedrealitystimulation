#include "ShapeGLES2.h"



ShapeGLES2::ShapeGLES2(Shape shape):
    vertices_(formToVerticesGLES2.at(shape.form_)),
    color_(colorToColorGLES2.at(shape.color_)),
    position_(positionToPositionGLES2.at(shape.position_)),
    renderMethod_(formToRendermethodGLES2.at(shape.form_)),
    scalingFactor_(shape.scalingFactor_),
    renderStatus_(true) {

}



std::vector<GLfloat>
ShapeGLES2::computeGLES2Vertices() {
    std::vector<GLfloat> transformedVertices = vertices_;
    for(auto i = 0; i < transformedVertices.size(); ++i) {
        transformedVertices[i] *= scalingFactor_;
        if(i%3 == 0) transformedVertices[i] += position_[0];
        if(i%3 == 1) transformedVertices[i] += position_[1];
    }
    return transformedVertices;
}

std::array<GLfloat, 4> ShapeGLES2::getGLES2Color() {return color_;}

GLenum ShapeGLES2::getGLES2RenderMethod() {return renderMethod_;}

bool ShapeGLES2::getRenderStatus() {return renderStatus_;}

void ShapeGLES2::setRenderStatus(bool newStatus) {renderStatus_ = newStatus;}
