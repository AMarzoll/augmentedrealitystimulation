#ifndef NDKAUGMENTEDREALITYSTIMULATION_SHAPEMAPSGLES2_H
#define NDKAUGMENTEDREALITYSTIMULATION_SHAPEMAPSGLES2_H

#include <map>

#include "../../stimulation/shape/Shape.h"

const std::map<ShapeForm, std::vector<GLfloat>> formToVerticesGLES2 = {
        {ShapeForm::TRIANGLE,{-1.0F, -1.0F,  0.0F,
                               1.0F, -1.0F,  0.0F,
                               0.0F,  1.0F,  0.0F}},
        {ShapeForm::SQUARE,  {-1.0F, -1.0F,  0.0F,
                              -1.0F,  1.0F,  0.0F,
                               1.0F, -1.0F,  0.0F,
                               1.0F,  1.0F,  0.0F}},
        {ShapeForm::CIRCLE,  { 0.0F,       0.0F,     0.0F,
                                      1.0F,       0.0F,     0.0F,
                                      0.9781F,    0.2079F,  0.0F,
                                      0.9135F,    0.4067F,  0.0F,
                                      0.8090F,    0.5878F,  0.0F,
                                      0.6691F,    0.7431F,  0.0F,
                                      0.5000F,    0.8660F,  0.0F,
                                      0.3090F,    0.9511F,  0.0F,
                                      0.1045F,    0.9945F,  0.0F,
                                      -0.1045F,    0.9945F,  0.0F,
                                      -0.3090F,    0.9511F,  0.0F,
                                      -0.5000F,    0.8660F,  0.0F,
                                      -0.6691F,    0.7431F,  0.0F,
                                      -0.8090F,    0.5878F,  0.0F,
                                      -0.9135F,    0.4067F,  0.0F,
                                      -0.9781F,    0.2079F,  0.0F,
                                      -1.0000F,    0.0000F,  0.0F,
                                      -0.9781F,   -0.2079F,  0.0F,
                                      -0.9135F,   -0.4067F,  0.0F,
                                      -0.8090F,   -0.5878F,  0.0F,
                                      -0.6691F,   -0.7431F,  0.0F,
                                      -0.5000F,   -0.8660F,  0.0F,
                                      -0.3090F,   -0.9511F,  0.0F,
                                      -0.1045F,   -0.9945F,  0.0F,
                                      0.1045F,   -0.9945F,  0.0F,
                                      0.3090F,   -0.9511F,  0.0F,
                                      0.5000F,   -0.8660F,  0.0F,
                                      0.6691F,   -0.7431F,  0.0F,
                                      0.8090F,   -0.5878F,  0.0F,
                                      0.9135F,   -0.4067F,  0.0F,
                                      0.9781F,   -0.2079F,  0.0F,
                                      1.0F,      -0.0F,     0.0F}},
        {ShapeForm::PENTAGON,  { 0.0000F,  0.0000F, 0.0000F,
                                      0.0000F,	1.0000F, 0.0000F,
                                      0.9511F,	0.3090F, 0.0F,
                                      0.5878F, -0.8090F, 0.0F,
                                      -0.5878F, -0.8090F, 0.0F,
                                      -0.9511F,	0.3090F, 0.0F,
                                      0.0F,	    1.0F,    0.0F}},
        {ShapeForm::CROSS,     { 0.0F,     0.0F,    0.0F,
                                      0.3333F,  1.0F,    0.0F,
                                      0.3333F,  0.3333F, 0.0F,
                                      1.0F,     0.3333F, 0.0F,
                                      1.0F,    -0.3333F, 0.0F,
                                      0.3333F, -0.3333F, 0.0F,
                                      0.3333F, -1.0F,    0.0F,
                                      -0.3333F, -1.0F,    0.0F,
                                      -0.3333F, -0.3333F, 0.0F,
                                      -1.0F,    -0.3333F, 0.0F,
                                      -1.0F,     0.3333F, 0.0F,
                                      -0.3333F,  0.3333F, 0.0F,
                                      -0.3333F,  1.0F,    0.0F,
                                      0.3333F,  1.0F,    0.0F}},
        {ShapeForm::STAR,      { 0.0000F,	0.0000F,	0.0000F,
                                      0.0000F,	1.0000F,	0.0000F,
                                      0.2939F,	0.4045F,	0.0000F,
                                      0.9511F,	0.3090F,	0.0000F,
                                      0.4755F,	-0.1545F,	0.0000F,
                                      0.5878F,	-0.8090F,	0.0000F,
                                      0.0000F,	-0.5000F,	0.0000F,
                                      -0.5878F,	-0.8090F,	0.0000F,
                                      -0.4755F,	-0.1545F,	0.0000F,
                                      -0.9511F,	0.3090F,	0.0000F,
                                      -0.2939F,	0.4045F,	0.0000F,
                                      -0.0000F,	1.0000F,	0.0000F,
                                      0.2939F,	0.4045F,	0.0000F}},
        {ShapeForm::SEMICIRCLE,{ 0.0F,       -0.5F,     0.0F,
                                      1.0F,       -0.5F,     0.0F,
                                      0.9781F,    -0.2903F,  0.0F,
                                      0.9135F,    -0.0933F,  0.0F,
                                      0.8090F,    0.0878F,  0.0F,
                                      0.6691F,    0.2431F,  0.0F,
                                      0.5000F,    0.3660F,  0.0F,
                                      0.3090F,    0.4511F,  0.0F,
                                      0.1045F,    0.4945F,  0.0F,
                                      -0.1045F,    0.4945F,  0.0F,
                                      -0.3090F,    0.4511F,  0.0F,
                                      -0.5000F,    0.3660F,  0.0F,
                                      -0.6691F,    0.2431F,  0.0F,
                                      -0.8090F,    0.0878F,  0.0F,
                                      -0.9135F,   -0.0933F,  0.0F,
                                      -0.9781F,   -0.2921F,  0.0F,
                                      -1.0000F,   -0.5000F,  0.0F}},
        {ShapeForm::CRESCENT, {-1.0F, -1.0F,  0.0F,
                                      1.0F, -1.0F,  0.0F,
                                      0.0F,  1.0F,  0.0F}}
};


const std::map<ShapeForm, GLenum> formToRendermethodGLES2 = {
        {ShapeForm::TRIANGLE,   GL_TRIANGLES},
        {ShapeForm::SQUARE,     GL_TRIANGLE_STRIP},
        {ShapeForm::CIRCLE,     GL_TRIANGLE_FAN},
        {ShapeForm::CRESCENT,   GL_TRIANGLE_FAN},
        {ShapeForm::CROSS,      GL_TRIANGLE_FAN},
        {ShapeForm::PENTAGON,   GL_TRIANGLE_FAN},
        {ShapeForm::SEMICIRCLE, GL_TRIANGLE_FAN},
        {ShapeForm::STAR,       GL_TRIANGLE_FAN}
};


const std::map<ShapeColor, std::array<GLfloat, 4>> colorToColorGLES2 = {
        {ShapeColor::RED,           {1.0F, 0.0F, 0.0F, 1.0F}},
        {ShapeColor::GREEN,         {0.0F, 1.0F, 0.0F, 1.0F}},
        {ShapeColor::BLUE,          {0.0F, 0.0F, 1.0F, 1.0F}},
        {ShapeColor::YELLOW,        {1.0F, 1.0F, 0.0F, 1.0F}},
        {ShapeColor::PURPLE,        {1.0F, 0.0F, 1.0F, 1.0F}},
        {ShapeColor::ORANGE,        {1.0F, 0.5F, 0.0F, 1.0F}},
        {ShapeColor::CYAN,          {0.0F, 1.0F, 1.0F, 1.0F}},
        {ShapeColor::WHITE,         {1.0F, 1.0F, 1.0F, 1.0F}},
        {ShapeColor::DARK_GREEN,    {0.13F, 0.55F, 0.13F, 1.0F}},
        {ShapeColor::BROWN,         {0.55F, 0.27F, 0.08F, 1.0F}}
};


const std::map<ShapePosition, std::array<GLfloat, 3>> positionToPositionGLES2 = { //TODO: turn into function
        {ShapePosition::UPPER_LEFT,     {-0.6F,  0.6F,  0.0F}},
        {ShapePosition::UPPER_RIGHT,    { 0.6F,  0.6F,  0.0F}},
        {ShapePosition::LOWER_LEFT,     {-0.6F, -0.6F,  0.0F}},
        {ShapePosition::LOWER_RIGHT,    { 0.6F, -0.6F,  0.0F}},
        {ShapePosition::CENTER_LEFT,    {-0.8F,  0.0F,  0.0F}},
        {ShapePosition::CENTER_RIGHT,   { 0.8F,  0.0F,  0.0F}},
        {ShapePosition::UPPER_MID,      { 0.0F,  0.8F,  0.0F}},
        {ShapePosition::LOWER_MID,      { 0.0F, -0.8F,  0.0F}},

        {ShapePosition::HEPTAGON1,      { 0.0F,   0.8F,  0.0F}}, //distance of vertices to center = 0.8
        {ShapePosition::HEPTAGON2,      { 0.63F,  0.5F,  0.0F}},
        {ShapePosition::HEPTAGON3,      { 0.78F, -0.18F, 0.0F}},
        {ShapePosition::HEPTAGON4,      { 0.35F, -0.72F, 0.0F}},
        {ShapePosition::HEPTAGON5,      {-0.35F, -0.72F, 0.0F}},
        {ShapePosition::HEPTAGON6,      {-0.78F, -0.18F, 0.0F}},
        {ShapePosition::HEPTAGON7,      {-0.63F,  0.5F,  0.0F}}
};



#endif //NDKAUGMENTEDREALITYSTIMULATION_SHAPEMAPSGLES2_H
