#include <android/log.h>
#include <android_native_app_glue.h>

#include "AugmentedRealityStimulation.h"
#include <thread>

void
android_main(struct android_app* ptrApp) {
    Log::info("Number of available hardware threads: %u", std::thread::hardware_concurrency());
    AugmentedRealityStimulation(ptrApp).run();
}
