#include "TimerServices.h"
#include <time.h>

int64_t
TimerServices::getClockRes() {
    timespec time ={0,0};
    clock_getres(clocktype_, &time);
    return static_cast<int64_t>(time.tv_sec) * 1e9 +
           static_cast<int64_t>(time.tv_nsec);
}



int64_t
TimerServices::getCurrentTime() {
    timespec time = {0,0};
    clock_gettime(clocktype_, &time);
    return static_cast<int64_t>(time.tv_sec) * 1e9 +
           static_cast<int64_t>(time.tv_nsec);
}