#ifndef NDKAUGMENTEDREALITYSTIMULATION_FILEOPERATIONS_H
#define NDKAUGMENTEDREALITYSTIMULATION_FILEOPERATIONS_H

#include <memory>
#include "android_native_app_glue.h"

std::unique_ptr<char, decltype(free)*> loadAssetFile(android_app* ptrApp, const std::string& filename, int& bufSz);


#endif //NDKAUGMENTEDREALITYSTIMULATION_FILEOPERATIONS_H
