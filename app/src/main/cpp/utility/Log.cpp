#include "Log.h"

#include <cstdarg>
#include <android/log.h>



void
Log::debug(const char* msg, ...) {
    va_list varArgs;
    va_start(varArgs, msg);
    __android_log_vprint(ANDROID_LOG_DEBUG, "AR-STIM", msg, varArgs);
    __android_log_print(ANDROID_LOG_DEBUG, "AR-STIM", "\n");
    va_end(varArgs);
}



void
Log::error(const char* msg, ...) {
    va_list varArgs;
    va_start(varArgs, msg);
    __android_log_vprint(ANDROID_LOG_ERROR, "AR-STIM", msg, varArgs);
    __android_log_print(ANDROID_LOG_ERROR, "AR-STIM", "\n");
    va_end(varArgs);
}



void
Log::info(const char* msg, ...) {
    va_list varArgs;
    va_start(varArgs, msg);
    __android_log_vprint(ANDROID_LOG_INFO, "AR-STIM", msg, varArgs);
    __android_log_print(ANDROID_LOG_INFO, "AR-STIM", "\n");
    va_end(varArgs);
}



void
Log::warn(const char* msg, ...) {
    va_list varArgs;
    va_start(varArgs, msg);
    __android_log_vprint(ANDROID_LOG_WARN, "AR-STIM", msg, varArgs);
    __android_log_print(ANDROID_LOG_WARN, "AR-STIM", "\n");
    va_end(varArgs);
}

