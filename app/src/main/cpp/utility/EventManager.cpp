#include "EventManager.h"
#include "Log.h"

EventManager::EventManager(android_app* ptrApp, ActivityHandler& activityHandler):
ptrApp_(ptrApp),
isEnabled_(false),
isQuitting_(false),
activityHandler_(activityHandler) {
    ptrApp_->userData = this;
    ptrApp_->onAppCmd = cb_appEvent;
}



void
EventManager::run() {
    //int32_t pollResult = -1;
    int32_t events = -1;
    android_poll_source* source = nullptr;

    Log::info("Starting event loop");
    while (true) {
        while ((ALooper_pollAll(isEnabled_ ? 0 : -1,
                                nullptr,
                                &events,
                                (void**) &source)) >= 0) {
            if (source != nullptr) {
                Log::info("Processing an event");
                source->process(ptrApp_, source);
            }
            if (ptrApp_->destroyRequested) {
                Log::info("Exiting event loop");
                return;
            }
        }
        if((isEnabled_) && (!isQuitting_)) {
            if(activityHandler_.onStep() != STATUS_OK) {
                isQuitting_ = true;
                ANativeActivity_finish(ptrApp_->activity);
            }
        }
    }
}



void
EventManager::activate() {
    //enables activity only if a window is available
    Log::info("Entered EventLoop::activate");
    if((!isEnabled_) && (ptrApp_->window != nullptr)) {
        isQuitting_ = false;
        isEnabled_ = true;
        if(activityHandler_.onActivate() != STATUS_OK) {
            isQuitting_ = true;
            this->deactivate();
            ANativeActivity_finish(ptrApp_->activity);
        }
    }
}



void
EventManager::deactivate() {
    Log::info("Entered EventLoop::deactivate");
    if(isEnabled_) {
        activityHandler_.onDeactivate();
        isEnabled_ = false;
    }
}



void
EventManager::cb_appEvent(android_app *ptrApp, int32_t cmd) {
    EventManager& eventLoop = *(EventManager*) ptrApp->userData;
    eventLoop.processAppEvent(cmd);
}



void
EventManager::processAppEvent(int32_t cmd) {
    switch(cmd) {
        case APP_CMD_CONFIG_CHANGED:
            activityHandler_.onConfigurationChanged();
            break;
        case APP_CMD_INIT_WINDOW:
            activityHandler_.onCreateWindow();
            break;
        case APP_CMD_DESTROY:
            activityHandler_.onDestroy();
            break;
        case APP_CMD_GAINED_FOCUS:
            this->activate();
            activityHandler_.onGainFocus();
            break;
        case APP_CMD_LOST_FOCUS:
            activityHandler_.onLostFocus();
            this->deactivate();
            break;
        case APP_CMD_LOW_MEMORY:
            activityHandler_.onLowMemory();
            break;
        case APP_CMD_PAUSE:
            activityHandler_.onPause();
            this->deactivate();
            break;
        case APP_CMD_RESUME:
            activityHandler_.onResume();
            break;
        case APP_CMD_SAVE_STATE:
            activityHandler_.onSaveInstanceState(&ptrApp_->savedState, &ptrApp_->savedStateSize);
            break;
        case APP_CMD_START:
            activityHandler_.onStart();
            break;
        case APP_CMD_STOP:
            activityHandler_.onStop();
            break;
        case APP_CMD_TERM_WINDOW:
            activityHandler_.onDestroyWindow();
            this->deactivate();
            break;
        default:
            break;
    }
}