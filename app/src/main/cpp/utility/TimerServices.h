#ifndef NDKAUGMENTEDREALITYSTIMULATION_TIMERSERVICES_H
#define NDKAUGMENTEDREALITYSTIMULATION_TIMERSERVICES_H

#include <cstdint>
#include <ctime>

class TimerServices {
public:
    static int64_t getCurrentTime();
    static int64_t getClockRes();
private:
    static constexpr clock_t clocktype_ = CLOCK_MONOTONIC;
};


#endif //NDKAUGMENTEDREALITYSTIMULATION_TIMERSERVICES_H
