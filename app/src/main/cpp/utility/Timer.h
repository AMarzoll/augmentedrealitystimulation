#ifndef NDKAUGMENTEDREALITYSTIMULATION_TIMER_H
#define NDKAUGMENTEDREALITYSTIMULATION_TIMER_H

#include <chrono>

class Timer {
public:
    void start();
    void stop();
    std::chrono::microseconds elapsed() const;
private:
    bool isActive_ = false;
    std::chrono::high_resolution_clock::time_point startTime_;
};


#endif //NDKAUGMENTEDREALITYSTIMULATION_TIMER_H
