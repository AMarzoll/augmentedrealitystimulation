#include "Timer.h"
#include "../utility/Log.h"


void
Timer::start() {
    startTime_ = std::chrono::high_resolution_clock::now();
    isActive_ = true;
}



void
Timer::stop() {
    isActive_ = false;
}



std::chrono::microseconds
Timer::elapsed() const {
    if (isActive_) {
        return std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::high_resolution_clock::now() - startTime_);
    } else {
        Log::error("Timer::elapsed(): Attempting to request time from inactive timer.");
        return std::chrono::microseconds::zero();
    }
}


