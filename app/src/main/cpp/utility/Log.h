#ifndef NDKAUGMENTEDREALITYSTIMULATION_LOG_H
#define NDKAUGMENTEDREALITYSTIMULATION_LOG_H

class Log {
public:
    static void debug(const char* msg, ...);
    static void error(const char* msg, ...);
    static void info(const char* msg, ...);
    static void warn(const char* msg, ...);
};

#ifndef NDEBUG
#define Log_debug(...) Log::debug(__VA_ARGS__)
#else
#define Log_debug(...)
#endif


#endif //NDKAUGMENTEDREALITYSTIMULATION_LOG_H
