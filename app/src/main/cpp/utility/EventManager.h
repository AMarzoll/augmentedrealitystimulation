#ifndef NDKAUGMENTEDREALITYSTIMULATION_EVENTMANAGER_H
#define NDKAUGMENTEDREALITYSTIMULATION_EVENTMANAGER_H

#include <android_native_app_glue.h>
#include "../ActivityHandler.h"

class EventManager {
public:
    EventManager(android_app* ptrApp, ActivityHandler& activityHandler);
    void run();

private:
    void activate();
    void deactivate();

    void processAppEvent(int32_t cmd);
    static void cb_appEvent(android_app* ptrApp, int32_t cmd);

    android_app* ptrApp_;
    bool isEnabled_;
    bool isQuitting_;
    ActivityHandler& activityHandler_;
};
#endif
