#include "FileOperations.h"
#include "Log.h"

#include <string>

std::unique_ptr<char, decltype(free)*>
loadAssetFile(android_app* ptrApp, const std::string& filename, int& bufSz)
{
    bufSz = 0;
    AAssetManager* assetManager = ptrApp->activity->assetManager;
    AAsset* asset = nullptr;
    asset = AAssetManager_open(assetManager, filename.c_str(), AASSET_MODE_UNKNOWN);
    if(asset == nullptr) {
        Log::error("Could not open asset.");
        return {nullptr, nullptr};
    }
    bufSz = AAsset_getLength(asset);
    auto buffer = std::unique_ptr<char, decltype(free)*>{static_cast<char*>(malloc(sizeof(char)*bufSz)), free}; //static_cast oder reinterpret_cast??
    if(buffer == nullptr) {
        Log::error("Could not acquire buffer");
        AAsset_close(asset);
        return {nullptr, nullptr};
    }
    Log::info("Opened asset and acquired buffer");
    AAsset_read(asset, buffer.get(), bufSz);
    AAsset_close(asset);
    return buffer;
}