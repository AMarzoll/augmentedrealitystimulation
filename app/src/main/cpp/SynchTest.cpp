#include "SynchTest.h"
#include "utility/Log.h"
#include <jni.h>

SynchTest::SynchTest(android_app *ptrApp):
        ptrApp_(ptrApp) {
}



void SynchTest::StartJavaChoreographer() {
    JNIEnv* jni = nullptr;
    ptrApp_->activity->vm->AttachCurrentThread(&jni, NULL);
    // Intiate Java Chreographer API.
    jclass clazz = jni->GetObjectClass(ptrApp_->activity->clazz);
    jmethodID methodID = jni->GetMethodID(clazz, "startChoreographer", "()V");
    jni->CallVoidMethod(ptrApp_->activity->clazz, methodID);
    ptrApp_->activity->vm->DetachCurrentThread();
    return;
}



void __cdecl SynchTest::StopJavaChoreographer() {
    JNIEnv* jni;
    ptrApp_->activity->vm->AttachCurrentThread(&jni, NULL);
    // Intiate Java Chreographer API.
    jclass clazz = jni->GetObjectClass(ptrApp_->activity->clazz);
    jmethodID methodID = jni->GetMethodID(clazz, "stopChoreographer", "()V");
    jni->CallVoidMethod(ptrApp_->activity->clazz, methodID);
    ptrApp_->activity->vm->DetachCurrentThread();
    // Make sure the render thread is not blocked.
    //cv_.notify_one();
    return;
}


extern "C"
JNIEXPORT void JNICALL Java_com_amarzoll_augmentedRealityStimulation_MyNativeActivity_choreographerCallback (JNIEnv* env, jobject obj, jlong t) {
    Log::info("C++ frame callback at time %u", t);
    return;
}