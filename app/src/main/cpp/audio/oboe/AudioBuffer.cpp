#include "AudioBuffer.h"

#include <algorithm>
#include <vector>
#include <cstdint>
#include <fstream>
#include <cinttypes>

#include "../../utility/Log.h"
#include "../../utility/TimerServices.h"


AudioBuffer::AudioBuffer() {
    buffer_.resize(nSamples_);
    readCopy_.resize(nSamples_);
}



//does not run in main thread, but in the one launched by the Oboe InputStream
void
AudioBuffer::write(void* ptrData, int32_t nData) {
    //Log::info("AudioBuffer::writeNext: Writing to buffer segment %i", writeIndex_);
    static int64_t last = 0;
    {
        std::lock_guard<std::mutex> lk(bufferMutex_);
        if(writeIndex_ % 100 == 0) {
            int64_t current = TimerServices::getCurrentTime();
            Log::info("AudioBuffer::write() at index %u: deltaTime: %" PRIi64, writeIndex_,
                      current - last);
            last = current;
        }
        memcpy(&buffer_[0], ptrData, nData * sizeof(int16_t));
        ++writeIndex_;
    }
}



std::vector<int16_t>
AudioBuffer::read() {
    static int64_t last = 0;
    if(readIndex_ % 100 == 0) {
        int64_t current = TimerServices::getCurrentTime();
        Log::info("AudioBuffer::read() at index %u: deltaTime: %" PRIi64, readIndex_,
                  current - last);
        last = current;
    }
    {
        std::lock_guard<std::mutex> lk(bufferMutex_);
        std::copy(buffer_.cbegin(), buffer_.cend(), readCopy_.begin());
        ++readIndex_;
    }
    return readCopy_; //TODO: überprüfen, ob das tut was es soll
}



void
AudioBuffer::writeDataToLog(int offsetStart, int offsetEnd) {
    std::ofstream ofs;
    ofs.open("/sdcard/log.txt", std::ofstream::out | std::ofstream::app);
    if (ofs.fail()) {
        Log::error("Error opening file stream");
        return;
    }
    ofs << "START" << '\n';
    for (int i = offsetStart; i < offsetEnd; ++i) {
        ofs << buffer_[i] << "\n";
    }
    ofs << "END" << '\n';
    ofs.close();
}


