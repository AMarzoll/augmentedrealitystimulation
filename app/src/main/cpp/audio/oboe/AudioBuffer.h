#ifndef NDKAUGMENTEDREALITYSTIMULATION_AUDIOBUFFER_H
#define NDKAUGMENTEDREALITYSTIMULATION_AUDIOBUFFER_H

#include <cstddef>
#include <array>
#include <vector>
#include "../../GlobalParameters.h"

class AudioBuffer {
public:
                                AudioBuffer();
    void                        write(void* ptrData, int32_t nData);
    std::vector<int16_t>        read();
    void                        writeDataToLog(int offsetStart, int offsetEnd);
    friend class SoundManagerOboe;

private:
    static constexpr int32_t    nSamples_ = Global::nSignalSamples;
    std::vector<int16_t>        buffer_;
    mutable std::mutex          bufferMutex_;
    std::vector<int16_t>        readCopy_;
    mutable uint64_t            readIndex_ = 0;
    mutable uint64_t            writeIndex_ = 0;
};


#endif //NDKAUGMENTEDREALITYSTIMULATION_AUDIOBUFFER_H
