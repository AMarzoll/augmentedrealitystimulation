#ifndef NDKAUGMENTEDREALITYSTIMULATION_INPUTSTREAMOBOE_H
#define NDKAUGMENTEDREALITYSTIMULATION_INPUTSTREAMOBOE_H

#include <array>
#include <oboe/Oboe.h>
#include <cstring>
#include <fstream>
#include <cstdint>
#include <vector>
#include <type_traits>

#include "../../GlobalParameters.h"
#include "../../utility/Log.h"
#include "AudioBuffer.h"

class InputStreamOboe: public oboe::AudioStreamCallback {
public:
                                    InputStreamOboe(AudioBuffer& audioRingBuffer);
    //void                            writeDataToLog() const;

    friend class SoundManagerOboe;

private:
    //oboe-callbacks
    oboe::DataCallbackResult    onAudioReady(oboe::AudioStream* oboeStream, void* audioData, int32_t numFrames) override;
    void                        onErrorBeforeClose (oboe::AudioStream* oboeStream, oboe::Result error) override;
    void                        onErrorAfterClose (oboe::AudioStream* oboeStream, oboe::Result error) override;

    void                        closeStream();

    static constexpr uint32_t   nSamplesPerCallback_ = Global::nSignalSamples;
    static constexpr uint32_t   sampleRate_ = Global::samplingFrequencyHz;

    AudioBuffer&                audioBuffer_;
    std::vector<float>          audioData_;
    mutable bool                isReadAlready_ = false;
    oboe::ManagedStream         inStream_;

};



#endif //NDKAUGMENTEDREALITYSTIMULATION_INPUTSTREAMOBOE_H
