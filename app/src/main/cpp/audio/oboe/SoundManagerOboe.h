#ifndef NDKAUGMENTEDREALITYSTIMULATION_SOUNDMANAGEROBOE_H
#define NDKAUGMENTEDREALITYSTIMULATION_SOUNDMANAGEROBOE_H

#include <vector>

#include "AudioBuffer.h"
#include "InputStreamOboe.h"


class SoundManagerOboe {
public:
                                SoundManagerOboe();
    void                        start();
    void                        update();
    void                        stop();
    std::vector<int16_t>        getAudio();
    bool                        isNewDataAvailable();

private:
    AudioBuffer audioBuffer_;
    InputStreamOboe inputStreamOboe_;
};


#endif //NDKAUGMENTEDREALITYSTIMULATION_SOUNDMANAGEROBOE_H
