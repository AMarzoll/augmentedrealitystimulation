#include "InputStreamOboe.h"


InputStreamOboe::InputStreamOboe(AudioBuffer& audioRingBuffer) :
        audioBuffer_(audioRingBuffer),
        audioData_(nSamplesPerCallback_) {
    Log::info("Calling InputStreamOboe::InputStreamOboe()");
    oboe::AudioStreamBuilder inStreamBuilder;
    //
    inStreamBuilder.getDeviceId();
    //inStreamBuilder.setSharingMode(oboe::SharingMode::Shared)
    inStreamBuilder.setSharingMode(oboe::SharingMode::Exclusive)
                  ->setPerformanceMode(oboe::PerformanceMode::LowLatency)
                  ->setChannelCount(oboe::ChannelCount::Mono)
                  ->setSampleRate(sampleRate_)
                  ->setFormat(oboe::AudioFormat::I16)
                  ->setCallback(this)
                  ->setDirection(oboe::Direction::Input)
                  ->setFramesPerCallback(nSamplesPerCallback_)
                  ->setBufferCapacityInFrames(sampleRate_)
                  ->setAudioApi(oboe::AudioApi::OpenSLES)
                  ->setSampleRateConversionQuality(oboe::SampleRateConversionQuality::None);
    oboe::Result streamStartResult = inStreamBuilder.openManagedStream(inStream_);
    if (streamStartResult != oboe::Result::OK) {
        Log::info("streamStartResult: %s", oboe::convertToText(streamStartResult));
        return;
    }
    Log::info("InputStreamOboe: AudioStream sharing mode: %s", oboe::convertToText(inStream_->getSharingMode()));
    Log::info("InputStreamOboe: AudioStream performance mode: %s", oboe::convertToText(inStream_->getPerformanceMode()));
    Log::info("InputStreamOboe: AudioStream channel count: %i", inStream_->getChannelCount());
    Log::info("InputStreamOboe: AudioStream sample rate: %i", inStream_->getSampleRate());
    Log::info("InputStreamOboe: AudioStream format: %s", oboe::convertToText(inStream_->getFormat()));
    Log::info("InputStreamOboe: AudioStream direction: %s", oboe::convertToText(inStream_->getDirection()));
    Log::info("InputStreamOboe: AudioStream frames per callback: %i", inStream_->getFramesPerCallback());
    Log::info("InputStreamOboe: AudioStream buffer capacity in frames: %i", inStream_->getBufferCapacityInFrames());
    Log::info("InputStreamOboe: AudioStream audio API: %s", oboe::convertToText(inStream_->getAudioApi()));
    Log::info("InputStreamOboe: AudioStream sample rate conversion quality: %i", inStream_->getSampleRateConversionQuality());
    inStream_->requestStart();
}



oboe::DataCallbackResult
InputStreamOboe::onAudioReady(oboe::AudioStream* audioStream, void* audioData, int32_t numFrames) {
    audioBuffer_.write(audioData, numFrames);
    return oboe::DataCallbackResult::Continue;
}



void
InputStreamOboe::closeStream() {
    inStream_->close();
}



void
InputStreamOboe::onErrorBeforeClose(oboe::AudioStream *oboeStream, oboe::Result error) {
    Log::info("Calling InputStreamOboe::onErrorBeforeClose");
    AudioStreamCallback::onErrorBeforeClose(oboeStream, error);
}



void
InputStreamOboe::onErrorAfterClose(oboe::AudioStream *oboeStream, oboe::Result error) {
    Log::info("Calling InputStreamOboe::onErrorAfterClose");
    AudioStreamCallback::onErrorAfterClose(oboeStream, error);
    Log::info("%s %s", __FILE__, __FUNCTION__);
}




