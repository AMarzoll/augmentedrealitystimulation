#include "SoundManagerOboe.h"

SoundManagerOboe::SoundManagerOboe() :
        audioBuffer_(),
        inputStreamOboe_(audioBuffer_) {
}



void
SoundManagerOboe::start() {

}



void
SoundManagerOboe::update() {

}



void
SoundManagerOboe::stop() {
    inputStreamOboe_.inStream_->close();
}



std::vector<int16_t>
SoundManagerOboe::getAudio() {
    return audioBuffer_.read();
}



bool
SoundManagerOboe::isNewDataAvailable() {
    std::lock_guard lk(audioBuffer_.bufferMutex_);
    return audioBuffer_.readIndex_ < audioBuffer_.writeIndex_;
}
