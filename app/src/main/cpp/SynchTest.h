#ifndef NDKAUGMENTEDREALITYSTIMULATION_SYNCHTEST_H
#define NDKAUGMENTEDREALITYSTIMULATION_SYNCHTEST_H

#include "android_native_app_glue.h"

class SynchTest {
public:
    SynchTest(android_app* ptrApp);
    void StartJavaChoreographer();
    void StopJavaChoreographer();

private:
    android_app* ptrApp_ = nullptr;
};

#endif //NDKAUGMENTEDREALITYSTIMULATION_SYNCHTEST_H
