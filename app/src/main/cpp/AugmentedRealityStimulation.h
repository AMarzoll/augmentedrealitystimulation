#ifndef NDKAUGMENTEDREALITYSTIMULATION_AUGMENTEDREALITYSTIMULATION_H
#define NDKAUGMENTEDREALITYSTIMULATION_AUGMENTEDREALITYSTIMULATION_H

#include <android_native_app_glue.h>
#include "ActivityHandler.h"
#include "utility/EventManager.h"
#include "stimulation/StimulationManager.h"
#include "graphics/GLES2/GraphicsManagerGLES2.h"
#include "audio/oboe/SoundManagerOboe.h"


class AugmentedRealityStimulation : public ActivityHandler {
public:

                        AugmentedRealityStimulation(android_app* ptrApp);
    void                run();

protected:

    Status              onActivate() override;
    void                onDeactivate() override;
    Status              onStep() override;

    void                onStart() override;
    void                onResume() override;

    void                onPause() override;
    void                onStop() override;

    void                onDestroy() override;

    void                onSaveInstanceState(void** ptrData, size_t* ptrSize) override;
    void                onConfigurationChanged() override;
    void                onLowMemory() override;

    void                onCreateWindow() override;
    void                onDestroyWindow() override;

    void                onGainFocus() override;
    void                onLostFocus() override;

private:
    android_app*                        ptrApp_;
    EventManager                        eventManager_;
    GraphicsManagerGLES2                graphicsManager_;
    SoundManagerOboe                    soundManager_;
    StimulationManager                  stimulationManager_;
};


#endif //NDKAUGMENTEDREALITYSTIMULATION_AUGMENTEDREALITYSTIMULATION_H
