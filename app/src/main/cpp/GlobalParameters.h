#ifndef NDKAUGMENTEDREALITYSTIMULATION_GLOBALPARAMETERS_H
#define NDKAUGMENTEDREALITYSTIMULATION_GLOBALPARAMETERS_H

#include <cstdlib>
#include <vector>
#include <cmath>
#include <array>
#include <complex>

using Status = int32_t;
using VecCF = std::vector<std::complex<float>>;

using StimulusID = std::string;
using DetectorID = uint8_t;
using TrackerID = uint8_t;
using ShapeGLES2ID = uint8_t;
using WaveletID = uint8_t;


constexpr Status STATUS_OK      = 0;
constexpr Status STATUS_BAD     = -1;
constexpr Status STATUS_EXIT    = -2;

namespace Global {

// audio parameters

    constexpr uint32_t samplingFrequencyHz = 48000;
    //constexpr float frameDurSec = 0.0166667;
    constexpr float frameDurSec = 0.03333;
// DSP and ML parameters

    constexpr uint16_t nConvolutionThreads = 2;
    constexpr std::size_t nWavelets = 18; //number of wavelets per detector
    constexpr uint16_t nTimeSteps = 6; //number of wavelet convolution epochs included in NN inference
    auto waveletFrequencies = [](float f0){ //returns required wavelet convolution frequencies per tone detector
        return std::array<float, nWavelets>{
                f0 * 1.0f, //fundamental frequency
                f0 * 2.0f, //first overtone
                f0 * 0.5f, //half f0
                f0 * 0.25f, //quarter f0
                f0 * 1.5f, //integerPlusHalf f0
                f0 * 2.5f, //integerPlusHalf f0 2
                f0 * std::pow(std::pow(2.0f,1.0f/12.0f), -6.0f), //neighbors
                f0 * std::pow(std::pow(2.0f,1.0f/12.0f), -5.0f),
                f0 * std::pow(std::pow(2.0f,1.0f/12.0f), -4.0f),
                f0 * std::pow(std::pow(2.0f,1.0f/12.0f), -3.0f),
                f0 * std::pow(std::pow(2.0f,1.0f/12.0f), -2.0f),
                f0 * std::pow(std::pow(2.0f,1.0f/12.0f), -1.0f),
                f0 * std::pow(std::pow(2.0f,1.0f/12.0f), 1.0f),
                f0 * std::pow(std::pow(2.0f,1.0f/12.0f), 2.0f),
                f0 * std::pow(std::pow(2.0f,1.0f/12.0f), 3.0f),
                f0 * std::pow(std::pow(2.0f,1.0f/12.0f), 4.0f),
                f0 * std::pow(std::pow(2.0f,1.0f/12.0f), 5.0f),
                f0 * std::pow(std::pow(2.0f,1.0f/12.0f), 6.0f)
        };
    };
    constexpr float frequencyTolerance = 0.02; //create new wavelet if frequency of existing ones is not within this percent of kernel frequency
    constexpr uint16_t nGaussEnvelope = 20;  //wavelet parameter, governs approximate number of cycles before taper-off
    constexpr std::size_t fftVecSize = 4096/2;  //must be power of two to exploit faster FFT algorithm
    constexpr VecCF::size_type nSignalSamples = (samplingFrequencyHz * frameDurSec) + 0.5f; //round(samplingFrequencyHz * frameDurSec)
    constexpr VecCF::size_type nKernelSamples = fftVecSize - nSignalSamples + 1;

    const std::string modelFilename = "mein_modell.tflite";
    constexpr float trainingDataConvMean = 6.75f;
    constexpr float trainingDataConvStd = 0.363f;
    constexpr float trainingDataLoudMean = -0.674f;
    constexpr float trainingDataLoudStd = 1.73f;

}
#endif //NDKAUGMENTEDREALITYSTIMULATION_GLOBALPARAMET