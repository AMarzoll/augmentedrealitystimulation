#include "AugmentedRealityStimulation.h"

#include "utility/TimerServices.h"
#include "utility/Log.h"
#include "audio/oboe/SoundManagerOboe.h"


AugmentedRealityStimulation::AugmentedRealityStimulation(android_app* ptrApp) :
    ptrApp_(ptrApp),
    eventManager_(ptrApp, *this),
    graphicsManager_(GraphicsManagerGLES2(ptrApp_)),
    stimulationManager_(ptrApp_, graphicsManager_, soundManager_)
    {
}



void
AugmentedRealityStimulation::run() {
    eventManager_.run();
}



Status
AugmentedRealityStimulation::onActivate() {
    Log::info("ARS::onActivate() entered");
    //initialize all managers
    graphicsManager_.registerStimulationManager(&stimulationManager_);
    stimulationManager_.start();
    //soundManager_->start();
    Log::info("Finished ARS::onActivate");
    return STATUS_OK;
}



void
AugmentedRealityStimulation::onDeactivate() {
    //deactivate all managers
    graphicsManager_.stop();
    soundManager_.stop();
}



Status
AugmentedRealityStimulation::onStep() {
    stimulationManager_.update();
    graphicsManager_.update();
    soundManager_.update();
    return STATUS_OK;
}



void
AugmentedRealityStimulation::onStart() {
//    ActivityHandler::onStart();
}



void
AugmentedRealityStimulation::onResume() {
    //ActivityHandler::onResume();
}



void
AugmentedRealityStimulation::onPause() {
//    ActivityHandler::onPause();
}



void
AugmentedRealityStimulation::onStop() {
//    ActivityHandler::onStop();
}



void
AugmentedRealityStimulation::onDestroy() {
//    ActivityHandler::onDestroy();
}



void
AugmentedRealityStimulation::onSaveInstanceState(void** ptrData, size_t* ptrSize) {
//    ActivityHandler::onSaveInstanceState(ptrData, ptrSize);
}



void
AugmentedRealityStimulation::onConfigurationChanged() {
//    ActivityHandler::onConfigurationChanged();
}



void
AugmentedRealityStimulation::onLowMemory() {
//    ActivityHandler::onLowMemory();
}



void
AugmentedRealityStimulation::onCreateWindow() {
    if(ptrApp_->window != nullptr) {
        graphicsManager_.start();
    }
//    ActivityHandler::onCreateWindow();
}



void
AugmentedRealityStimulation::onDestroyWindow() {
//    ActivityHandler::onDestroyWindow();
}



void
AugmentedRealityStimulation::onGainFocus() {
//    ActivityHandler::onGainFocus();
}



void
AugmentedRealityStimulation::onLostFocus() {
//    ActivityHandler::onLostFocus();
}
