#include "Detector.h"
#include "WaveletConvolutionManager.h"

#include "../stimulation/Stimulus.h"
#include "../utility/Log.h"


Detector::Detector()
{
    for(uint16_t i = 0; i < Global::nTimeSteps; ++i) {
        convolutionResults_.push_front(std::vector<float>(0.0f, Global::nWavelets + 1));
    }
}



void
Detector::init(float f0Hz) {
    requiredFrequenciesHz_ = Global::waveletFrequencies(f0Hz);
}



void
Detector::registerWaveletConvolution(WaveletID waveletID) {
    wavelets_.push_back(waveletID);
}



void
Detector::setDetectorID(DetectorID detectorID) {
    detectorID_ = detectorID;
}



void
Detector::requestInference(
        WaveletConvolutionManager& waveletConvolutionManager,
        NeuralNetworkInference& neuralNetworkInference) {
    //fetch
    std::vector<float> curConvolutionResult;
    for(auto w : wavelets_) {
        curConvolutionResult.push_back(waveletConvolutionManager.getConvolutionResult(w));
        //curConvolutionResult.insert(curConvolutionResult.begin(), waveletConvolutionManager.getConvolutionResult(w));
    }
    float curLoudnessFactor = waveletConvolutionManager.getcurLoudnessFactor();

    //normalize convolution features and loudness factor (separately)
    std::transform(
            curConvolutionResult.begin(),
            curConvolutionResult.end(),
            curConvolutionResult.begin(),
            [](float x){return (x - Global::trainingDataConvMean) / Global::trainingDataConvStd;}
    );
    curLoudnessFactor = (curLoudnessFactor - Global::trainingDataLoudMean) / Global::trainingDataLoudStd;
    //Log::info("curLoudnessFactor: %f", curLoudnessFactor);
    curLoudnessFactor += 1.0f;
    static double allInfs = 0;
    static double nans = 0;
    allInfs++;
    if(std::isnan(curLoudnessFactor)) {
        ++nans;
    }

    //concatenate loudness factor, remove oldest result from queue and add most recent one
    curConvolutionResult.push_back(curLoudnessFactor);
    convolutionResults_.push_front(curConvolutionResult);
    convolutionResults_.pop_back();

    //unwrap queue for NN inference
    std::vector<float> convolutionResultsChained;
    for(const auto& v : convolutionResults_) {
        convolutionResultsChained.insert(convolutionResultsChained.end(), v.cbegin(), v.cend());
    }

    float inferenceResult = neuralNetworkInference.performInference(convolutionResultsChained);
    //Log::info("Inference result for Detector with f0 = %f: %f", requiredFrequenciesHz_[0], inferenceResult);
    nnInfResult_ = inferenceResult >= 0.5;
}

std::array<float, Global::nWavelets> Detector::getFrex() {
    return requiredFrequenciesHz_;
}

DetectorID Detector::getDetectorID() {return detectorID_;}


