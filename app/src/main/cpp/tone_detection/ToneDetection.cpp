#include "ToneDetection.h"

#include "../stimulation/StimulationManager.h"

ToneDetection::ToneDetection(android_app* ptrApp, StimulationManager& stimulationManager):
stimulationManager_(stimulationManager),
neuralNetworkInference_(ptrApp),
waveletConvolutionManager_(stimulationManager){

}



void
ToneDetection::registerDetector(DetectorID detectorID) {
    //Log::info("Entering ToneDetection::registerDetector");
    detectors_.push_back(detectorID);
}



struct DetectionThreadFailure : std::exception{};

void
ToneDetection::launchDetectionThread() {
    //Log::info("Entering ToneDetection::launchDetectionThread");
    detectionThread_ = std::thread (&ToneDetection::doDetection, this);
    if(detectionThread_.joinable()){
        detectionThread_.detach();
    } else {
        throw DetectionThreadFailure();
    }

}



using namespace std::chrono_literals;

[[noreturn]] void
ToneDetection::doDetection() {
    while(true) {
        //Log::info("New loop cycle in ToneDetection::doDetection");
        //poll-check for new sound data
        while (!stimulationManager_.isNewAudioAvailable()) {
            std::this_thread::sleep_for(250us);
        }
        //acquire sound data copy
        std::vector<int16_t> rawAudio = stimulationManager_.getAudio();

        //preprocess sound data (to complex float, zero-pad, FFT)
        waveletConvolutionManager_.prepareSignal(rawAudio);

        //notify convolution threads
        waveletConvolutionManager_.resetConvolutionCounter();
        waveletConvolutionManager_.preprocessedSignalCondVar_.notify_all();

        //wait for convolution to finish
        while(std::any_of(
                waveletConvolutionManager_.convolutionsCompleteFlags_.cbegin(),
                waveletConvolutionManager_.convolutionsCompleteFlags_.cend(),
                [](bool x){return !x;})) {
            std::this_thread::sleep_for(250us);
            //waveletConvolutionManager_.preprocessedSignalCondVar_.notify_all();
        }

        //order Detector objects to perform NN inference
        for(auto id : detectors_) {
            stimulationManager_.accessDetector(id).requestInference(waveletConvolutionManager_, neuralNetworkInference_);
        }

        //notify main thread
        detectionComplete_ = true;
    }
}