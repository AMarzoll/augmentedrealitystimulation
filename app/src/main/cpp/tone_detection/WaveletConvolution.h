#ifndef NDKAUGMENTEDREALITYSTIMULATION_WAVELETCONVOLUTION_H
#define NDKAUGMENTEDREALITYSTIMULATION_WAVELETCONVOLUTION_H

#include "../GlobalParameters.h"
#include <iostream>
#include <cmath>
#include <vector>
#include <complex>


class WaveletConvolution
{
public:
                        WaveletConvolution(float kernelFrequencyHz);
                        WaveletConvolution() = delete;
    void                convolve(const VecCF& signalFft);
    float               getConvolutionResultSum() const {return convolutionResultSum_;}
    float               getKernelFrequencyHz() const {return kernelFrequencyHz_;}
    VecCF::size_type    getConvolutionSize() const {return nConvolutionSize_;}

private:
    const float             kernelFrequencyHz_;
    const uint32_t          samplingFrequencyHz_ = Global::samplingFrequencyHz;
    const uint16_t          nGaussEnvelope_ = Global::nGaussEnvelope;
    const float             sGaussEnvelope_;
    const VecCF::size_type  nKernelSamples_ = Global::nKernelSamples;
    const VecCF::size_type  nHalfKernelSamples_;
    const VecCF::size_type  nSignalSamples_ = Global::nSignalSamples;
    const VecCF::size_type  nConvolutionSize_;

    VecCF                   kernel_;
    VecCF                   kernelFft_;
    VecCF                   convolutionResult_;

    float                   convolutionResultSum_;
};

#endif //NDKAUGMENTEDREALITYSTIMULATION_WAVELETCONVOLUTION_H
