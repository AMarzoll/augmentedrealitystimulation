#ifndef NDKAUGMENTEDREALITYSTIMULATION_TONEDETECTION_H
#define NDKAUGMENTEDREALITYSTIMULATION_TONEDETECTION_H

#include <android_native_app_glue.h>
#include <vector>
#include <thread>
#include <atomic>

//#include "../stimulation/StimulationManager.h"
#include "WaveletConvolutionManager.h"
#include "NeuralNetworkInference.h"
#include "../GlobalParameters.h"

class StimulationManager;

class ToneDetection {
    friend class StimulationManager;
public:
    ToneDetection(android_app* ptrApp, StimulationManager& stimulationManager);
    //void init();

private:
    void registerDetector(DetectorID detectorID); //registers individual Detector components of Stimulus objects with this class
    void launchDetectionThread(); //launches detection thread
    [[noreturn]] void doDetection();

    StimulationManager& stimulationManager_;

    WaveletConvolutionManager waveletConvolutionManager_;
    NeuralNetworkInference neuralNetworkInference_;

    std::vector<DetectorID> detectors_;
    std::thread detectionThread_;

    std::atomic_bool detectionComplete_ = false;
};


#endif //NDKAUGMENTEDREALITYSTIMULATION_TONEDETECTION_H
