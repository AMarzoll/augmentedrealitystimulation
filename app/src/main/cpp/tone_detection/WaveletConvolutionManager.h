#ifndef NDKAUGMENTEDREALITYSTIMULATION_WAVELETCONVOLUTIONMANAGER_H
#define NDKAUGMENTEDREALITYSTIMULATION_WAVELETCONVOLUTIONMANAGER_H

#include <vector>
#include <map>
#include <thread>
#include <shared_mutex>
#include <condition_variable>
#include <atomic>
#include <tuple>

#include "WaveletConvolution.h"
#include "../GlobalParameters.h"

class StimulationManager;

class WaveletConvolutionManager {
    friend class ToneDetection;
    friend class StimulationManager;
public:
    WaveletConvolutionManager(StimulationManager& stimulationManager);
    float getConvolutionResult(WaveletID waveletID);
    float getcurLoudnessFactor() const;
private:
    void prepareSignal(const std::vector<int16_t>& rawSignal);
    void createConvolutions(DetectorID detectorID);
    void launchWorkerThreads();
    [[noreturn]] void convolutionWorkerFunction(std::vector<WaveletID> waveletIDs, uint8_t threadIdx);
    void resetConvolutionCounter();

    std::map<WaveletID, WaveletConvolution>         waveletConvolutions_;
    std::vector<std::thread>                        convolutionWorkerThreads_;
    StimulationManager&                             stimulationManager_;

    std::atomic_bool                                preprocessingComplete_ = false;
    VecCF                                           preprocessedSignal_;
    std::shared_mutex                               preprocessedSignalMtx_;
    std::condition_variable_any                     preprocessedSignalCondVar_;
    std::atomic_int                                 nConvolutionThreadsDone_ = Global::nConvolutionThreads; //for first time
    std::array<std::atomic_bool, Global::nConvolutionThreads> convolutionsCompleteFlags_;
    float                                           curLoudnessFactor_;

    WaveletID                                       nextID_ = 0;
};


#endif //NDKAUGMENTEDREALITYSTIMULATION_WAVELETCONVOLUTIONMANAGER_H
