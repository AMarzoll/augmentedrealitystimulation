#ifndef NDKAUGMENTEDREALITYSTIMULATION_NEURALNETWORKINFERENCE_H
#define NDKAUGMENTEDREALITYSTIMULATION_NEURALNETWORKINFERENCE_H

#include <string>
#include <memory>
#include <vector>
#include <android_native_app_glue.h>

#include <tensorflow/lite/interpreter.h>
#include <tensorflow/lite/model.h>
#include "../GlobalParameters.h"

class NeuralNetworkInference {
public:
    NeuralNetworkInference(android_app* ptrApp);
    float performInference(const std::vector<float>& features);

private:
    void loadModelFromAssets();
    void makeInterpreter();

    android_app* ptrApp_ = nullptr;
    std::string modelFilename_ = Global::modelFilename;
    std::unique_ptr<tflite::FlatBufferModel> model_ = nullptr;
    std::unique_ptr<tflite::Interpreter> interpreter_ = nullptr;
    std::unique_ptr<char, decltype(free)*> modelBuffer_ = {nullptr, free}; //buffer must be kept alive even after building the model!!
};


#endif //NDKAUGMENTEDREALITYSTIMULATION_NEURALNETWORKINFERENCE_H
