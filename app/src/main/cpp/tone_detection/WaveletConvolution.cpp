#include "WaveletConvolution.h"

#include <algorithm>
#include <numeric>
#include <complex>

#include "FftComplex.h"
#include "../utility/Log.h"


WaveletConvolution::WaveletConvolution(float kernelFrequencyHz):
        kernelFrequencyHz_(kernelFrequencyHz),
        sGaussEnvelope_(nGaussEnvelope_ / (2.0f * M_PI * kernelFrequencyHz_)),
        nHalfKernelSamples_(static_cast<VecCF::size_type>(std::floor(nKernelSamples_ / 2.0f))),
        nConvolutionSize_(nKernelSamples_ + nSignalSamples_ - 1) {
    float kernelDuration = static_cast<float>(nKernelSamples_) / static_cast<float>(samplingFrequencyHz_);
    float t0 = -kernelDuration / 2.0f;
    std::vector<float> time(nKernelSamples_, 0.0f);
    kernel_.resize(nKernelSamples_);
    for(auto i = 0; i < time.size(); ++i) {
        time[i] = t0 + i * (1.0f / static_cast<float>(samplingFrequencyHz_));
    }
    // Lambda functions for kernel calculation
    auto gaussEnvelope = [=](float t) -> std::complex<float> {
        return std::exp((-1.0f * std::pow(t, 2.0f)) / (2.0f * std::pow(sGaussEnvelope_, 2.0f)));
    };
    std::complex<float> imagUnit(0.0f, 1.0f);
    auto complexSine = [=](float t) -> std::complex<float> {
        return std::exp(imagUnit * 2.0f * static_cast<float>(M_PI) * kernelFrequencyHz_ * t);
    };
    // Calculate kernel values
    for(VecCF::size_type i = 0; i < kernel_.size(); ++i) {
        kernel_[i] = gaussEnvelope(time[i]) * complexSine(time[i]);
    }
    // Pre-calculate FFT of zero-padded kernel for faster convolution

    if((nConvolutionSize_ & (nConvolutionSize_ - 1)) != 0) { //bit magic
        Log::warn("Warning: convolution vector size is not a power of two.\n");
    }
    convolutionResult_.resize(nConvolutionSize_);
    std::fill(convolutionResult_.begin(), convolutionResult_.end(), std::complex<float>(0.0, 0.0));
    kernelFft_.insert(kernelFft_.begin(), kernel_.begin(), kernel_.end());
    kernelFft_.insert(kernelFft_.end(), nConvolutionSize_ - kernel_.size(), std::complex<float>(0.0, 0.0));
    Fft::transform(kernelFft_);
}



void
WaveletConvolution::convolve(const VecCF& signalFft)
{
    //Log::info("Entering WaveletConvolution::convolve");
    //element-wise multiplication of kernel and signal FFTs, as per convolution theorem
    std::transform(
            kernelFft_.cbegin(),
            kernelFft_.cend(),
            signalFft.cbegin(),
            convolutionResult_.begin(),
            std::multiplies<>{}
            );

    Fft::inverseTransform(convolutionResult_);

    //post-processing: take sum, scale by frequency and take log10 (for feeding into ANN)
    convolutionResultSum_ = std::accumulate(
            convolutionResult_.cbegin(),
            convolutionResult_.cend(),
            0.0f,
            [](std::complex<float> x1, std::complex<float> x2){return std::abs(x1) + std::abs(x2);}
            );
    convolutionResultSum_ *= kernelFrequencyHz_;
    convolutionResultSum_ = std::log10f(convolutionResultSum_);
    //Log::info("F = %f, conv = %f\n", kernelFrequencyHz_, convolutionResultSum_);
    // do not scale by number of elements here (nConvolutionSize_) and do not prune result to original signal size
}