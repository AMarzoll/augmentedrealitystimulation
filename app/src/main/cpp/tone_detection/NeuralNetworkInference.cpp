#include "NeuralNetworkInference.h"

#include "android/asset_manager.h"
#include "../utility/Log.h"
#include <tensorflow/lite/optional_debug_tools.h>
#include <tensorflow/lite/kernels/register.h>

#include "../utility/FileOperations.h"


#define TFLITE_MINIMAL_CHECK(x)                                 \
  if (!(x)) {                                                   \
    fprintf(stderr, "Error at %s:%d\n", __FILE__, __LINE__);    \
    exit(1);                                                    \
  }



NeuralNetworkInference::NeuralNetworkInference(android_app* ptrApp) :
        ptrApp_(ptrApp) {
    loadModelFromAssets();
    makeInterpreter();
}



void
NeuralNetworkInference::loadModelFromAssets() {
    Log::info("NnInference::loadModelFromAssets(): Attempting to load tensorflow lite model file.");
    int bufSz = 0;
    modelBuffer_ = loadAssetFile(ptrApp_, modelFilename_, bufSz);
    Log::info("Model buffer size: %i", bufSz);
    model_ = tflite::FlatBufferModel::VerifyAndBuildFromBuffer(modelBuffer_.get(), bufSz);
    if(!model_) {
        Log::error("Could not build tflite model from buffer");
    }
}



void
NeuralNetworkInference::makeInterpreter() {
    Log::info("Entering NeuralNetworkInference::makeInterpreter");
    tflite::ops::builtin::BuiltinOpResolver resolver;
    tflite::InterpreterBuilder builder(*model_, resolver);
    builder(&interpreter_);
    if(!interpreter_) {
        Log::error("Could not build tflite interpreter from model");
        return;
    }
    interpreter_->UseNNAPI(false);
    // Allocate tensor buffers.
    TFLITE_MINIMAL_CHECK(interpreter_->AllocateTensors() == kTfLiteOk);
    tflite::PrintInterpreterState(interpreter_.get());

    Log::info("Size of input tensor: %u\n", interpreter_->inputs().size());
    Log::info("Size of output tensor: %u\n", interpreter_->outputs().size());
}



void myPrintInterpreterState(tflite::Interpreter* interpreter);

float NeuralNetworkInference::performInference(const std::vector<float>& features) {
    //Log::info("Entering NeuralNetworkInference::performInference");
    //auto* rawPtr = interpreter_.get();
    //myPrintInterpreterState(rawPtr);

    /*
    auto* inputTensor = interpreter_->typed_input_tensor<float>(0);
    if(inputTensor == nullptr) {
        return -1.0;
    }
    *inputTensor = features[0];
    */

    auto* ptr = interpreter_->typed_input_tensor<float>(0);
    for(int i = 0; i < features.size(); ++i) {
        *ptr = features[i];
        ++ptr;
        /*if(i == 0 || i == features.size()-1) {
            Log::info("Address of input tensor: %p", ptr);
        }*/
    }
    TfLiteStatus invokeStatus = interpreter_->Invoke();
    if(invokeStatus != kTfLiteOk) {
        Log::error("Invoke() failed!");
    }

    auto* out = interpreter_->typed_output_tensor<float>(0);
    //Log::info("Address of output tensor: %p", out);
    if(out == nullptr) {
        return -1;
    }
    return *out;
}



void PrintIntVector(const std::vector<int>& v) {
    for (const auto& it : v) {
        Log::info(" %d", it);
    }
    Log::info("\n");
}



void PrintTfLiteIntVector(const TfLiteIntArray* v) {
    if (!v) {
        Log::info(" (null)\n");
        return;
    }
    for (int k = 0; k < v->size; k++) {
        Log::info(" %d", v->data[k]);
    }
    Log::info("\n");
}



const char* TensorTypeName(TfLiteType type) {
    switch (type) {
        case kTfLiteNoType:
            return "kTfLiteNoType";
        case kTfLiteFloat32:
            return "kTfLiteFloat32";
        case kTfLiteInt32:
            return "kTfLiteInt32";
        case kTfLiteUInt8:
            return "kTfLiteUInt8";
        case kTfLiteInt8:
            return "kTfLiteInt8";
        case kTfLiteInt64:
            return "kTfLiteInt64";
        case kTfLiteString:
            return "kTfLiteString";
        case kTfLiteBool:
            return "kTfLiteBool";
        case kTfLiteInt16:
            return "kTfLiteInt16";
        case kTfLiteComplex64:
            return "kTfLiteComplex64";
        case kTfLiteFloat16:
            return "kTfLiteFloat16";
        case kTfLiteFloat64:
            return "kTfLiteFloat64";
    }
    return "(invalid)";
}



const char* AllocTypeName(TfLiteAllocationType type) {
    switch (type) {
        case kTfLiteMemNone:
            return "kTfLiteMemNone";
        case kTfLiteMmapRo:
            return "kTfLiteMmapRo";
        case kTfLiteDynamic:
            return "kTfLiteDynamic";
        case kTfLiteArenaRw:
            return "kTfLiteArenaRw";
        case kTfLiteArenaRwPersistent:
            return "kTfLiteArenaRwPersistent";
        case kTfLitePersistentRo:
            return "kTfLitePersistentRo";
    }
    return "(invalid)";
}



void myPrintInterpreterState(tflite::Interpreter* interpreter) {
    Log::info("Interpreter has %zu tensors and %zu nodes\n",
              interpreter->tensors_size(), interpreter->nodes_size());
    Log::info("Inputs:");
    PrintIntVector(interpreter->inputs());
    Log::info("Outputs:");
    PrintIntVector(interpreter->outputs());
    Log::info("\n");
    for (size_t tensor_index = 0; tensor_index < interpreter->tensors_size();
         tensor_index++) {
        TfLiteTensor* tensor = interpreter->tensor(static_cast<int>(tensor_index));
        Log::info("Tensor %3zu %-20s %10s %15s %10zu bytes (%4.1f MB) ", tensor_index,
                  tensor->name, TensorTypeName(tensor->type),
                  AllocTypeName(tensor->allocation_type), tensor->bytes,
                  (static_cast<float>(tensor->bytes) / (1 << 20)));
        PrintTfLiteIntVector(tensor->dims);
    }
    printf("\n");
    for (size_t node_index = 0; node_index < interpreter->nodes_size();
         node_index++) {
        const std::pair<TfLiteNode, TfLiteRegistration>* node_and_reg =
                interpreter->node_and_registration(static_cast<int>(node_index));
        const TfLiteNode& node = node_and_reg->first;
        const TfLiteRegistration& reg = node_and_reg->second;
        if (reg.custom_name != nullptr) {
            Log::info("Node %3zu Operator Custom Name %s\n", node_index,
                      reg.custom_name);
        } else {
            Log::info("Node %3zu Operator Builtin Code %3d %s\n", node_index,
                      reg.builtin_code, tflite::EnumNamesBuiltinOperator()[reg.builtin_code]);
        }
        Log::info("  Inputs:");
        PrintTfLiteIntVector(node.inputs);
        Log::info("  Outputs:");
        PrintTfLiteIntVector(node.outputs);
        if (node.intermediates && node.intermediates->size) {
            Log::info("  Intermediates:");
            PrintTfLiteIntVector(node.intermediates);
        }
        if (node.temporaries && node.temporaries->size) {
            Log::info("  Temporaries:");
            PrintTfLiteIntVector(node.temporaries);
        }
    }
}

