#include "WaveletConvolutionManager.h"
#include "../utility/Log.h"
#include "FftComplex.h"

#include "../stimulation/StimulationManager.h"
#include "Detector.h"

#include <algorithm>
#include <complex>
#include <numeric>
#include <thread>
#include <utility>


WaveletConvolutionManager::WaveletConvolutionManager(StimulationManager& stimulationManager):
stimulationManager_(stimulationManager){
    preprocessedSignal_.resize(Global::nKernelSamples + Global::nSignalSamples - 1);
}



void
WaveletConvolutionManager::createConvolutions(DetectorID detectorID) {
    //Log::info("Entering WaveletConvolutionManager::createConvolutions");
    Detector& detector = stimulationManager_.accessDetector(detectorID);
    auto frequencies = detector.getFrex();
    for(auto f : frequencies){
        //check if same wavelet already exists
        bool waveletExists = false;
        for(auto& w : waveletConvolutions_) {
            float curFreq = w.second.getKernelFrequencyHz();
            float lowerBound = curFreq - Global::frequencyTolerance * curFreq;
            float upperBound = curFreq + Global::frequencyTolerance * curFreq;
            if((f > lowerBound) && (f < upperBound)) {
                waveletExists = true;
                detector.registerWaveletConvolution(w.first);
                break;
            }
        }
        if(!waveletExists) {
            waveletConvolutions_.emplace(std::make_pair(nextID_, WaveletConvolution(f)));
            detector.registerWaveletConvolution(nextID_);
            ++nextID_;
        }

    }
}



void
WaveletConvolutionManager::launchWorkerThreads() {
    //Log::info("Entering WaveletConvolutionManager::launchWorkerThreads");
    int nThreads =  Global::nConvolutionThreads;
    int minConvolutionsPerThread = waveletConvolutions_.size() / nThreads;
    int remainder = waveletConvolutions_.size() % nThreads;
    uint8_t nextThreadIdx = 0;

    auto it = waveletConvolutions_.begin();

    for(int iThread = 0; iThread < nThreads; ++iThread) {
        int convolutionsForThread =
                (remainder > iThread) ? minConvolutionsPerThread + 1 : minConvolutionsPerThread;
        std::vector<WaveletID> ids;
        for(int iID = 0; iID < convolutionsForThread; ++iID) {
            ids.push_back(it->first);
            ++it;
        }
        convolutionWorkerThreads_.emplace_back(
                std::thread(&WaveletConvolutionManager::convolutionWorkerFunction, this, ids, nextThreadIdx)
                );
        ++nextThreadIdx;
        convolutionWorkerThreads_.rbegin()->detach();
    }
}



[[noreturn]] void
WaveletConvolutionManager::convolutionWorkerFunction(std::vector<WaveletID> waveletIDs, uint8_t threadIdx) {
    while(true) {
        //Log::info("New loop cycle in WaveletConvolutionManager::convolutionWorkerFunction");
        //wait for signal from ToneDetection object
        std::shared_lock lk(preprocessedSignalMtx_);
        preprocessedSignalCondVar_.wait(
                lk,
                [=](){
                    return (preprocessingComplete_.load() && !convolutionsCompleteFlags_[threadIdx].load());
                });
        //do convolutions
        for(auto wID : waveletIDs) {
            waveletConvolutions_.at(wID).convolve(preprocessedSignal_);
        }
        //do something to signal that convolutions are complete
        convolutionsCompleteFlags_[threadIdx].store(true);
    }
}



void
WaveletConvolutionManager::prepareSignal(const std::vector<int16_t>& rawSignal) {
    //Log::info("Entering WaveletConvolutionManager::prepareSignal");
    static const float int_16_max = 32767.0f;
    int16_t signalMaxAbs = *(std::max_element(
            rawSignal.cbegin(),
            rawSignal.cend(),
            [](int16_t x1, int16_t x2){return std::abs(x1) < std::abs(x2);}));
    signalMaxAbs = std::abs(signalMaxAbs);
    auto toComplexFloat = [=](int16_t x)->std::complex<float>{
        return std::complex<float>((x / (int_16_max * signalMaxAbs)), 0.0f);
    };
    std::lock_guard lk(preprocessedSignalMtx_);
    curLoudnessFactor_ = std::logf(static_cast<float>(signalMaxAbs) / int_16_max) / std::logf(10.0f);
    std::transform(
            rawSignal.cbegin(),
            rawSignal.cend(),
            preprocessedSignal_.begin(),
            toComplexFloat
            );

    //zero-padding: at the end yields the same results as putting zeros at both ends
    std::fill(
            preprocessedSignal_.begin()+Global::nSignalSamples,
            preprocessedSignal_.end(),
            std::complex<float>(0.0, 0.0)
            );
    //take FFT of signal (convolution theorem)
    Fft::transform(preprocessedSignal_);
    preprocessingComplete_.store(true);
}



struct LastConvolutionUnfinished : std::exception {};

void
WaveletConvolutionManager::resetConvolutionCounter() {
    //Log::info("Entering WaveletConvolutionManager::resetConvolutionCounter");
    if(all_of(convolutionsCompleteFlags_.cbegin(), convolutionsCompleteFlags_.cend(), [](bool x){return x;})){
        for(auto& e : convolutionsCompleteFlags_) {
            e.store(false);
        }
    } else {
        //throw LastConvolutionUnfinished();
        Log::info("Last convolution unfinished");
    }
}

float WaveletConvolutionManager::getConvolutionResult(WaveletID waveletID) {
    //Log::info("Entering WaveletConvolutionManager::getConvolutionResultSum");
    return waveletConvolutions_.at(waveletID).getConvolutionResultSum();
}

float WaveletConvolutionManager::getcurLoudnessFactor() const {
    return curLoudnessFactor_;
}