#ifndef NDKAUGMENTEDREALITYSTIMULATION_DETECTOR_H
#define NDKAUGMENTEDREALITYSTIMULATION_DETECTOR_H

#include <vector>
#include <deque>

#include "WaveletConvolutionManager.h"
#include "NeuralNetworkInference.h"
#include "../GlobalParameters.h"

class Stimulus;

//component of Stimulus
class Detector {
    friend class Stimulus;
public:
    Detector();
    void init(float f0Hz);

    std::array<float, Global::nWavelets> getFrex();
    void registerWaveletConvolution(WaveletID waveletID);
    DetectorID getDetectorID();

    void setDetectorID(DetectorID detectorID);

    void requestInference(
            WaveletConvolutionManager& waveletConvolutionManager,
            NeuralNetworkInference& neuralNetworkInference);

private:
    DetectorID detectorID_;
    std::vector<WaveletID> wavelets_;
    std::array<float, Global::nWavelets> requiredFrequenciesHz_;
    //std::vector<float> convolutionResults_;
    std::deque<std::vector<float>> convolutionResults_;
    bool nnInfResult_ = false;
};


#endif //NDKAUGMENTEDREALITYSTIMULATION_DETECTOR_H
